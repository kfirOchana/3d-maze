import static to_string_utils.Array.*;

public class ArrayTest {

	private static final int[][][] ARRAY_3D =
			new int[][][]{
					new int[][]{
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
					},
					new int[][]{
							new int[]{1, 0, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 0, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 0, 0, 0, 0, 0, 0, 1, 1},
							new int[]{1, 0, 1, 1, 1, 1, 0, 1, 1},
							new int[]{1, 0, 1, 1, 1, 1, 0, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
					},
					new int[][]{
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 0, 1, 1},
							new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1},
					}
			};


	@org.junit.Test
	public void toStringArray3d() throws Exception {
		System.out.print(array3dToString(ARRAY_3D));
	}

	@org.junit.Test
	public void toStringArray2d() throws Exception {
		System.out.print(array2dToString(ARRAY_3D[1]));
	}

	@org.junit.Test
	public void toStringArray1d() throws Exception {
		System.out.print(array1dToString(ARRAY_3D[1][1]));
	}

}