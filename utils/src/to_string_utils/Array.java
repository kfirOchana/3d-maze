package to_string_utils;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Array {

	@NotNull
    public static String array3dToString(int[][][] array3d) {

        return multiIntArrayToFormattedString(Arrays.stream(array3d), Array::array2dToString, ",\n");
    }

	@NotNull
    public static String array2dToString(int[][] array2d) {

        return multiIntArrayToFormattedString(Arrays.stream(array2d), Array::array1dToString, ",\n");
    }

	@NotNull
    public static String array1dToString(int[] array1d) {

        return indentAllLines(wrapWithBracket(join(
                Arrays.stream(array1d).
                        mapToObj(Integer::toString), ",")));
    }

    private static <T> String multiIntArrayToFormattedString(Stream<T> tStream,
                                                             Function<? super T, ? extends String> mapper,
                                                             String joinString) {

        return indentAllLines(wrapWithBracket(join(
                tStream.map(mapper), joinString)));
    }

    private static String join(Stream<String> stringStream, String put) {
        return stringStream.collect(Collectors.joining(put));
    }

    private static String indentAllLines(String lines) {
        return lines.replace("\n", "\n\t")
                .replaceFirst("\\{\\{", "{\n\t\\{")
                .replaceFirst("\\}\\}", "}\n}");
    }

	private static String wrapWithBracket(String string) {
        return "{" + string + "}";
    }
}
