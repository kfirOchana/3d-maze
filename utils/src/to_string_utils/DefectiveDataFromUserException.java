package to_string_utils;

class DefectiveDataFromUserException extends IllegalArgumentException {

	private static final String promptToUser = "Could not finish the process due to defective data.";

	DefectiveDataFromUserException() {
		super(promptToUser);
	}

	public DefectiveDataFromUserException(String message) {
		super(message);
	}

	public DefectiveDataFromUserException(Throwable cause) {
		super(cause);
	}

	public DefectiveDataFromUserException(String message, Throwable cause) {
		super(message, cause);
	}
}
