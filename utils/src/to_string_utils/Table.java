package to_string_utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Note that this Class uses String types only, in order to stay generic.
 */
public class Table {


	/**
	 * The number of spaces between the fields data and the separator.
	 */
	private static final int SPACE_ON_EDGE = 2;

	/**
	 * The separator character between two fields data.
	 */
	private static final char IN_LINE_HORIZONTAL_SEPARATOR = '|';

	/**
	 * The separator character between two $(VERTICAL_SEPARATOR) sequences.
	 */
	private static final char BETWEEN_LINES_HORIZONTAL_SEPARATOR = '+';

	/**
	 *
	 */
	private static final char NEW_LINE = '\n';

	/**
	 * The separator character between two lines of data.
	 */
	private static final String VERTICAL_SEPARATOR = "-";

	/**
	 * These HashMap will map between the column index to its width.
	 */
	private static final HashMap<Integer, Integer> alienWidthForFieldsMap = new HashMap<>();

	/**
	 * we will save the number of chart fields, for more readable code.
	 */
	private static int numberOfFields;

	/**
	 * same as mentioned before
	 */
	private static List<String> fieldsList;

	/**
	 * @param objectsDataList Rows list to print
	 * @param chartFieldsList The charts fields
	 * @throws DefectiveDataFromUserException If the data doesn't match to the chart's headline fields
	 */
	public static String buildChart(List<String> chartFieldsList, List<List<String>> objectsDataList)
			throws DefectiveDataFromUserException {

		fieldsList = chartFieldsList;   // Initializing the fields list, now when we have it from the user
		numberOfFields = fieldsList.size(); // Initializing the number of fields

        /* Finding and setting the optimize Align width so we will be able to print the charts properly (without leaking text)*/
		initializeAlignForFieldsMap(objectsDataList);

		StringBuilder table = new StringBuilder();

		table.append(buildTableRow(fieldsList));  // Printing the charts headline fields

		for (List<String> objectsData : objectsDataList) {  // Going through every object data
			table.append(buildTableRow(objectsData)); // Printing it in a row.
		}

		return table.toString();
	}

	/**
	 * @param listOfStringsTopPrint The list of data that we will print in the chart cells
	 * @throws DefectiveDataFromUserException If the data doesn't match to the chart's headline fields
	 */
	private static StringBuilder buildTableRow(List<String> listOfStringsTopPrint) throws DefectiveDataFromUserException {

		if (listOfStringsTopPrint.size() != numberOfFields) {
			// In a normal situation we shouldn't even ge to here. These use to make the class generic as possible
			throw new DefectiveDataFromUserException();
		}

		return printObjectsRow(listOfStringsTopPrint, IN_LINE_HORIZONTAL_SEPARATOR).append(   // Printing the data
	    /* Creating a list of separators {"--...-", "--...-", ..., "--...-"} and printing them between the BETWEEN_LINES_HORIZONTAL_SEPARATOR */
				printObjectsRow(generateVerticalSeparatorLine(), BETWEEN_LINES_HORIZONTAL_SEPARATOR));
	}

	/**
	 * @param listOfStringsToPrint The list of String that we are going to print in the cells
	 * @param inLineSeparator      The separator between two Strings (BETWEEN_LINES_HORIZONTAL_SEPARATOR / IN_LINE_HORIZONTAL_SEPARATOR)
	 */
	private static StringBuilder printObjectsRow(List<String> listOfStringsToPrint, char inLineSeparator) {

		StringBuilder row = new StringBuilder();

		for (int i = 0; i < numberOfFields; ++i) {  // Going through the index of all Strings
			final String stringToPrint = listOfStringsToPrint.get(i); // Saving the string into local variable for more readable code...

            /* We use the center function here in order to get a format String that will place the data and the separator right in the middle */

			if (i < numberOfFields - 1) { // If we are still in the middle of the line, we will print the row with a separator
				row.append(String.format(center(stringToPrint, alienWidthForFieldsMap.get(i)), stringToPrint, inLineSeparator));
			} else {    // else we will go down to a new line
				row.append(String.format(center(stringToPrint, alienWidthForFieldsMap.get(i)), stringToPrint, NEW_LINE));
			}
		}

		return row;
	}

	/**
	 * @return a list of separator sequences in length determine by alienWidthForFieldsMap.
	 * @abst {"--...-", "--...-", ..., "--...-"} with length numberOfFields,
	 * and each "--...-" with length alienWidthForFieldsMap.get(i)*2
	 * Thous separators will be print between the BETWEEN_LINES_HORIZONTAL_SEPARATOR.
	 */
	private static List<String> generateVerticalSeparatorLine() {
		List<String> verticalSeparatorLine = new ArrayList<>();    // Initializing the separators list
		for (int i = 0; i < numberOfFields; ++i) {   // going through every field index
            /* Creating a String of '-'s with length of alienWidthForFieldsMap.get(i)*2, and adding it to the list */
			verticalSeparatorLine.add(new String(new char[alienWidthForFieldsMap.get(i) * 2]).replace("\0", VERTICAL_SEPARATOR));
		}
		return verticalSeparatorLine;
	}

	/**
	 * In These function we are going though every column and searching for the longest String,
	 * and its length will be the column's width.
	 *
	 * @param personsDataList The charts data; each List<String> present a row and each String present single cell.
	 */
	private static void initializeAlignForFieldsMap(List<List<String>> personsDataList) {

		if (!personsDataList.isEmpty()) {

			for (int i = 0; i < numberOfFields; ++i) {  // Going through the index of every column
                /* Mapping the column index with the longest length of string */
				alienWidthForFieldsMap.put(i, getMaxLengthOfStringFromTheseFields(personsDataList, i) / 2 + SPACE_ON_EDGE);
			}
		}
	}

	/**
	 * @param personsDataList The charts data; each List<String> present a row and each String present single cell.
	 * @param columnIndex     The column index that we will search in
	 * @return The length of the longest String in the column
	 */
	private static int getMaxLengthOfStringFromTheseFields(List<List<String>> personsDataList, int columnIndex) {
		int maxLengthOfStringFromTheseField = fieldsList.get(columnIndex).length(); // Initializing the current longest String to the fields label
		for (List<String> somePersonData : personsDataList) {   // Going through every row, and checking if the String at the columnIndex is longer.
			maxLengthOfStringFromTheseField = Math.max(maxLengthOfStringFromTheseField, somePersonData.get(columnIndex).length());
		}
		return maxLengthOfStringFromTheseField;
	}

	/**
	 * @param word       The word that we want to print
	 * @param alignWidth The width of the cell that the word will be in
	 * @return format String that can use to center the word along with the 2nd character after it
	 */
	private static String center(String word, int alignWidth) {

		return "%" + (alignWidth + word.length() / 2) + "s" + "%" + (alignWidth - word.length() / 2 + 1) + "c";
	}
}
