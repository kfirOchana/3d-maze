package controller;


import controller.commands.ErrorfulCommand;
import model.Model;
import view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class GameController implements ModelControllerInterface, ViewControllerInterface {

	private static final String INVALID_ARGUMENTS_SET = "ERROR! Command fail due to invalid arguments set. try '%s'";

	private static final String IO_DISFUNCTION = "ERROR! Input/Output process failed";

	private static final String FILE_NOT_FOUND = "ERROR! Couldn't find the required file.";

	private final String KEYWORD;
	private final String ARGUMENTS;
	private final String HELP;

	protected final Model model;
	private final View view;

	void displayToView(Object commandOutput) {
		view.displayData(commandOutput.toString());
	}

	private static final Queue<Object> outputQueue = new LinkedBlockingQueue<>();


	protected GameController(Model model, View view,
	                          final String KEYWORD, final String ARGUMENTS, final String HELP) {
		this.model = model;
		this.view = view;

		this.KEYWORD = KEYWORD;
		this.ARGUMENTS = ARGUMENTS;
		this.HELP = HELP;
	}

	protected void wrapFunctionWithErrorHandler(ErrorfulCommand command, String[] arguments) {
		try {
			addOutput(command.run(arguments));

		} catch (FileNotFoundException e) {
			addOutput(FILE_NOT_FOUND);
			addOutput(String.format(INVALID_ARGUMENTS_SET, getCallFormat()));

		} catch (IOException e) {
			addOutput(IO_DISFUNCTION);
			addOutput(String.format(INVALID_ARGUMENTS_SET, getCallFormat()));

		} catch (IndexOutOfBoundsException | NumberFormatException e) {
			addOutput(String.format(INVALID_ARGUMENTS_SET, getCallFormat()));

		} catch (IllegalArgumentException e) {
			addOutput(e.getMessage());
			addOutput(String.format(INVALID_ARGUMENTS_SET, getCallFormat()));
		}
	}

	protected void addOutput(Object commandOutput) {
		Controller.outputQueue.add(commandOutput.toString());
	}

	private Object pollOutput() {
		return Controller.outputQueue.poll();
	}

	@Override
	public void updateModelChanges() {
		viewController.displayToView(pollOutput());
	}

	@Override
	public String getHelp() {
		return this.HELP;
	}

	@Override
	public String getCallFormat() {
		return KEYWORD + " " + ARGUMENTS;
	}

	public String getKeyword() {
		return KEYWORD;
	}

	public String getArguments() {
		return ARGUMENTS;
	}
}
