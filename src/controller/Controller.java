package controller;

import model.Model;
import view.View;


public class Controller extends Thread {

	protected final Model model;
	protected final View view;

	protected Controller(Model model, View view) {
		this.model = model;
		this.view = view;
	}
}
