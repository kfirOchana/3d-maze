package presenter;


import boot.PresenterProperties;
import javafx.util.Pair;
import model.Model;
import presenter.commands.Errors;
import view.View;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("unchecked")
public class GamePresenter implements Observer {

	protected static PresenterProperties defaultPresenterProperties;
	protected static HashMap<String, InteractiveCommand> keywordToCommandMap;
	protected final Model model;
	protected final View view;


	protected GamePresenter(Model model, View view) {
		this.model = model;
		this.view = view;
	}

	public GamePresenter(Model model, View view, HashMap<String, InteractiveCommand> keywordToCommandMap, PresenterProperties defaultPresenterProperties) {

		this.model = model;
		this.view = view;
		GamePresenter.keywordToCommandMap = keywordToCommandMap;
		GamePresenter.defaultPresenterProperties = defaultPresenterProperties;
	}


	@Override
	public synchronized void update(Observable o, Object arg) {

		if (o == view)
			handleViewNotification((Pair<String, String[]>) arg);

		else if (o == model)
			handleModelNotification(arg);
	}

	private void handleModelNotification(Object arg) {

		if (arg instanceof String)
			view.updateView((String) arg);

		else if (arg instanceof FileNotFoundException)
			view.updateView(Errors.FILE_NOT_FOUND);

		else if (arg instanceof IOException)
			view.updateView(Errors.IO_DISFUNCTION);
	}

	@SuppressWarnings("unchecked")
	private void handleViewNotification(Pair<String, String[]> arg) {

		final String commandKeyword = arg.getKey();
		final String[] arguments = arg.getValue();

		if (keywordToCommandMap.containsKey(commandKeyword))
			keywordToCommandMap.get(commandKeyword).doCommand(arguments);
		else
			view.updateView(String.format(Errors.UNKNOWN_COMMAND, commandKeyword));
	}
}
