package presenter;


public interface Command {

	void doCommand(String[] arguments);
}