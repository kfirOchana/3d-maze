package presenter.commands;


import model.Model;
import view.View;

public class MazeCrossSectionDisplay extends AbstractInteractiveCommand {

	private static final String HEIGHT_AXIS = "x";
	private static final String WIDTH_AXIS = "y";
	private static final String DEPTH_AXIS = "z";

	private static final String KEYWORD = "display_cross_section";
	private static final String ARGUMENTS = String.format("<name> <axis {'%s','%s','%s'}> <index>",
			HEIGHT_AXIS.toUpperCase(),
			WIDTH_AXIS.toUpperCase(),
			DEPTH_AXIS.toUpperCase());

	private static final String HELP = "Prints the 2D-Plane that created when fix firmly the <{X,Y,Z}> dimension to <index>";

	public MazeCrossSectionDisplay(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::getCrossSectionBy, arguments);
	}

	private Object getCrossSectionBy(String[] arguments) throws IndexOutOfBoundsException, IllegalArgumentException {

		String name = arguments[0];
		String dimension = arguments[1];
		int index = Integer.parseInt(arguments[2]);

		try {
			switch (dimension) {
				case HEIGHT_AXIS:
					return model.getCrossSectionByX(name, index);

				case WIDTH_AXIS:
					return model.getCrossSectionByY(name, index);

				case DEPTH_AXIS:
					return model.getCrossSectionByZ(name, index);

				default:
					throw new IllegalArgumentException(String.format(Errors.INVALID_FLAG, dimension));
			}
		} catch (IndexOutOfBoundsException e) {
			throw new IllegalArgumentException(String.format(Errors.INDEX_OUT_OF_MAZES_BOUND, index));
		}
	}
}
