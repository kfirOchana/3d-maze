package presenter.commands;


import maze_generators.GrowingTreeGenerator_LastSection;
import maze_generators.GrowingTreeGenerator_RandomSection;
import maze_generators.RandomizedKruskalsGenerator;
import maze_generators.SimpleMaze3dGenerator;
import model.Model;
import view.View;

public class MazeGeneration extends AbstractInteractiveCommand {

	private static final String GROWING_TREE_GENERATOR__LAST_SELECTION__ = "gtg_l";
	private static final String GROWING_TREE_GENERATOR__RANDOM_SELECTION__ = "gtg_r";
	private static final String SIMPLE_MAZE_GENERATOR = "smg";
    private static final String RANDOMIZED_KRUSKALS_GENERATOR = "rkg";

	private static final String KEYWORD = "generate_3d_maze";

    private static final String OPTIONAL_ARGUMENTS = String.format("<generation algorithm {'%s', '%s', '%s', '%s'}>",
            GROWING_TREE_GENERATOR__LAST_SELECTION__.toUpperCase(),
			GROWING_TREE_GENERATOR__RANDOM_SELECTION__.toUpperCase(),
            SIMPLE_MAZE_GENERATOR.toUpperCase(),
            RANDOMIZED_KRUSKALS_GENERATOR.toUpperCase());
    private static final String ALL_ARGUMENTS = "<name> <height> <width> <depth>" + String.format(OPTIONAL_ARGUMENTS_DESCRIBER, OPTIONAL_ARGUMENTS);

	private static final String GENERATING_MAZE = "Generating...";
	private static final String MAZE_GENERATION_COMPLETE = "maze %s is ready.";

	private static final String HELP = "Creates 3D-maze named <name> " +
			"with dimensions <height> X <width> X <depth>";

	public MazeGeneration(Model model, View view) {
		super(model, view, KEYWORD, ALL_ARGUMENTS, HELP);
	}

	public static String getOptionalArguments() {
		return OPTIONAL_ARGUMENTS;
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::generateMaze, arguments);
	}

	private Object generateMaze(String[] arguments) throws IndexOutOfBoundsException, IllegalArgumentException {

		String mazeName = arguments[0];
		int height = Integer.parseInt(arguments[1]);
		int width = Integer.parseInt(arguments[2]);
		int depth = Integer.parseInt(arguments[3]);

		String algorithm;
		if (arguments.length == 5)
			algorithm = arguments[4];
		else algorithm = defaultPresenterProperties.getDefaultMazeGenerationAlgorithm();

		switch (algorithm) {
			case GROWING_TREE_GENERATOR__LAST_SELECTION__:
				model.generateMaze(mazeName, height, width, depth,
                        new GrowingTreeGenerator_LastSection(),
                        String.format(MAZE_GENERATION_COMPLETE, mazeName));
				break;

			case GROWING_TREE_GENERATOR__RANDOM_SELECTION__:
				model.generateMaze(mazeName, height, width, depth,
                        new GrowingTreeGenerator_RandomSection(),
                        String.format(MAZE_GENERATION_COMPLETE, mazeName));
				break;

			case SIMPLE_MAZE_GENERATOR:
				model.generateMaze(mazeName, height, width, depth,
                        new SimpleMaze3dGenerator(),
                        String.format(MAZE_GENERATION_COMPLETE, mazeName));
                break;

            case RANDOMIZED_KRUSKALS_GENERATOR:
                model.generateMaze(mazeName, height, width, depth,
                        new RandomizedKruskalsGenerator(),
                        String.format(MAZE_GENERATION_COMPLETE, mazeName));
				break;

			default:
				throw new IllegalArgumentException(Errors.UNKNOWN_ALGORITHM);
		}
		return GENERATING_MAZE;
	}
}
