package presenter.commands;

import model.Model;
import presenter.InteractiveCommand;
import view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static to_string_utils.Table.buildChart;

public class Menu extends AbstractInteractiveCommand {

	private static final String KEYWORD = "menu";
	private static final String ARGUMENTS = "";

	private static final String HELP = "Prints the the available commands and use information.";

	private static final String COMMAND_TITLE = "COMMAND USAGE";
	private static final String DESCRIPTION_TITLE = "DESCRIPTION";

	public Menu(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::getMenu, arguments);
	}

	private Object getMenu(String[] arguments) throws IndexOutOfBoundsException {

		if (arguments.length != 0) throw new IndexOutOfBoundsException();

		List<String> tableFields = Arrays.asList(COMMAND_TITLE, DESCRIPTION_TITLE);
		List<List<String>> tableContent = new ArrayList<>();

        for (InteractiveCommand command : keywordToCommandMap.values())
            tableContent.add(Arrays.asList(command.getCallFormat(), command.getHelp()));

        return buildChart(tableFields, tableContent);
    }
}
