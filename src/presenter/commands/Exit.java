package presenter.commands;


import model.Model;
import view.View;

public class Exit extends AbstractInteractiveCommand {

	private static final String KEYWORD = "exit";
	private static final String ARGUMENTS = "";

	private static final String HELP = "Exit the program.";

	public Exit(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	public static String getExitKeyword() {
		return KEYWORD;
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::exit, arguments);
	}

	private Object exit(String[] arguments) {
		if (arguments.length != 0) throw new IndexOutOfBoundsException();

		model.close();
		view.close();

		// This return value will  not be printed
		return null;
	}
}
