package presenter.commands;

import model.Model;
import view.View;

import java.io.File;
import java.io.FileNotFoundException;

public class DirectoryLookup extends AbstractInteractiveCommand {

	private static final String KEYWORD = "dir";
	private static final String ARGUMENTS = "\"<path>\"";

	private static final String HELP = "Shows the files and folders found in <path> " +
			"(absolute\\relative path in the computer's storage system).";

	public DirectoryLookup(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::directoryLookup, arguments);
	}

	private Object directoryLookup(String[] arguments)
			throws FileNotFoundException, IndexOutOfBoundsException, IllegalArgumentException {

		if (arguments.length != 1) throw new IllegalArgumentException(Errors.PATH_CONTAIN_WHITE_SPACES);

		String path = arguments[0];

		File[] filesFromDirectory = model.getDirectoryContent(path);

		if (filesFromDirectory == null) throw new FileNotFoundException();

		StringBuilder filesName = new StringBuilder();
		for (File file : filesFromDirectory) {
			filesName.append(file.getName()).append('\n');
		}

		return filesName.toString();
	}
}
