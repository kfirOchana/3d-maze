package presenter.commands;


import model.Model;
import search.BestFirstSearch;
import search.DepthFirstSearch;
import view.View;

public class SolveMaze extends AbstractInteractiveCommand {

	private static final String KEYWORD = "solve";

	private static final String BEST_FIRST_SEARCH = "bfs";
	private static final String DEPTH_FIRST_SEARCH = "dfs";

	private static final String OPTIONAL_ARGUMENTS =
			String.format("<solve algorithm {'%s', '%s'}>",
					BEST_FIRST_SEARCH.toUpperCase(),
			DEPTH_FIRST_SEARCH.toUpperCase());
	private static final String ARGUMENTS = "<name>" + String.format(OPTIONAL_ARGUMENTS_DESCRIBER, OPTIONAL_ARGUMENTS);

	private static final String SOLVING_MAZE = "Solving...";
	private static final String MAZE_SOLVED_SUCCESSFULLY = "Maze %s has been solved successfully.";

	private static final String HELP = "Solves the maze named <name> in the background";

	public SolveMaze(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	public static String getOptionalArguments() {
		return OPTIONAL_ARGUMENTS;
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::solveMaze, arguments);
	}

	private String solveMaze(String[] arguments) {

		String mazeName = arguments[0];
		String algorithm;

		if (arguments.length == 2)
			algorithm = arguments[1];
		else algorithm = defaultPresenterProperties.getDefaultMazeSolveAlgorithm();

		switch (algorithm) {
			case BEST_FIRST_SEARCH:
				model.solveMaze(mazeName, new BestFirstSearch<>(),
						String.format(MAZE_SOLVED_SUCCESSFULLY, mazeName));
				break;

			case DEPTH_FIRST_SEARCH:
				model.solveMaze(mazeName, new DepthFirstSearch<>(),
						String.format(MAZE_SOLVED_SUCCESSFULLY, mazeName));
				break;

			default:
				throw new IllegalArgumentException(Errors.UNKNOWN_ALGORITHM);
		}
		return SOLVING_MAZE;
	}
}
