package presenter.commands;


import model.Model;
import view.View;

public class SaveMaze extends AbstractInteractiveCommand {

	private static final String KEYWORD = "save_maze";
	private static final String ARGUMENTS = "<name> \"<file path>\"";

	private static final String SAVING_MAZE = "Saving...";
	private static final String FILE_SAVED_SUCCESSFULLY = "Maze %s has saved successfully.";

	private static final String HELP = "Saves a compressed version of the maze named <name> in file.";

	public SaveMaze(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::saveMaze, arguments);
	}

	private Object saveMaze(String[] arguments) throws IndexOutOfBoundsException {

		if (arguments.length != 2) throw new IllegalArgumentException(Errors.PATH_CONTAIN_WHITE_SPACES);

		String mazeName = arguments[0];
		String filePath = arguments[1];

		model.saveMaze(mazeName, filePath, String.format(FILE_SAVED_SUCCESSFULLY, mazeName));
		return SAVING_MAZE;
	}
}
