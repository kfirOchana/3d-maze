package presenter.commands;


import model.Model;
import presenter.InteractiveCommand;
import view.View;

import java.io.FileNotFoundException;

public abstract class AbstractInteractiveCommand extends AbstractCommand implements InteractiveCommand {

    private final String KEYWORD;
    private final String ARGUMENTS;
    private final String HELP;

    public AbstractInteractiveCommand(Model model, View view, String KEYWORD, String ARGUMENTS, String HELP) {
        super(model, view);

        this.KEYWORD = KEYWORD;
        this.ARGUMENTS = ARGUMENTS;
        this.HELP = HELP;
    }

    @Override
    public String getHelp() {
        return this.HELP;
    }

    @Override
    public String getCallFormat() {
        return getKeyword() + " " + getArguments();
    }

    @Override
    public String getKeyword() {
        return KEYWORD;
    }

    @Override
    public String getArguments() {
        return ARGUMENTS;
    }

    /**
     * Handle exceptions that may be thrown from the Presenter layer (NOT model layer).
     *
     * @param command   Command to run.
     * @param arguments The match arguments for <code>command</code>.
     */
    void wrapFunctionWithErrorHandler(ErrorfulCommand command, String[] arguments) {
        try {
            Object returnValue = command.run(arguments);
            if (returnValue != null)
                updateView(returnValue);
            else throw new IllegalArgumentException(Errors.MAZE_NOT_FOUND);

        } catch (FileNotFoundException e) {
            view.updateView(Errors.FILE_NOT_FOUND);
            view.updateView(String.format(Errors.INVALID_ARGUMENTS_SET, getCallFormat()));

        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            view.updateView(String.format(Errors.INVALID_ARGUMENTS_SET, getCallFormat()));

        } catch (IllegalArgumentException e) {
            view.updateView(e.getMessage());
            view.updateView(String.format(Errors.INVALID_ARGUMENTS_SET, getCallFormat()));
        }
    }
}
