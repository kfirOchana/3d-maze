package presenter.commands;


public interface Errors {

	String INVALID_ARGUMENTS_SET = "ERROR! Command fail due to invalid arguments set. try '%s'";

	String INVALID_FLAG = "ERROR! invalid %s flag.";

	String IO_DISFUNCTION = "ERROR! Input/Output process failed";

	String FILE_NOT_FOUND = "ERROR! Couldn't find the required file.";

	String MAZE_NOT_FOUND = "ERROR! Couldn't find Maze with the specified name";

	String PATH_CONTAIN_WHITE_SPACES = "ERROR! path contains white spaces should be wrapped with quote signs (\").";

	String INDEX_OUT_OF_MAZES_BOUND = "ERROR! Index %d out of maze's bounds.";

	String UNKNOWN_ALGORITHM = "ERROR! The specified algorithm is not recognized.";

	String UNKNOWN_COMMAND = "ERROR! '%s' is not a recognized command, Enter 'menu' to get help.";
}
