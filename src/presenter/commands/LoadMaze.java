package presenter.commands;


import model.Model;
import view.View;

public class LoadMaze extends AbstractInteractiveCommand {

	private static final String KEYWORD = "load_maze";
	private static final String ARGUMENTS = "\"<file path>\" <name>";

	private static final String LOADING_MAZE = "Loading...";
	private static final String FILE_LOADED_SUCCESSFULLY = "Maze %s has loaded successfully.";
	private static final String HELP = "Loads from the file named <file name> new 3D-Maze.";

	public LoadMaze(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::loadMaze, arguments);
	}

	private Object loadMaze(String[] arguments) throws IndexOutOfBoundsException {

		if (arguments.length != 2) throw new IllegalArgumentException(Errors.PATH_CONTAIN_WHITE_SPACES);

		String path = arguments[0];
		String mazeName = arguments[1];

		model.loadMaze(path, mazeName, String.format(FILE_LOADED_SUCCESSFULLY, mazeName));

		return LOADING_MAZE;
	}
}
