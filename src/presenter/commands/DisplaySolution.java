package presenter.commands;


import model.Model;
import view.View;

public class DisplaySolution extends AbstractInteractiveCommand {

	private static final String KEYWORD = "display_solution";
	private static final String ARGUMENTS = "<name>";

	private static final String HELP = "Sows the steps towards victory.";

	public DisplaySolution(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::displaySolution, arguments);
	}

	private Object displaySolution(String[] arguments) throws IndexOutOfBoundsException {

		String mazeName = arguments[0];

		return model.getSolution(mazeName);
	}
}
