package presenter.commands;


import maze_generators.Maze3d;
import model.Model;
import position.Position;
import presenter.Command;
import presenter.GamePresenter;
import search.Solution;
import view.View;

import java.io.FileNotFoundException;
import java.util.Observable;

public abstract class AbstractCommand extends GamePresenter implements Command {

	static final String OPTIONAL_ARGUMENTS_DESCRIBER = " (Optionals: %s)";

	public AbstractCommand(Model model, View view) {

		super(model, view);
	}

	@SuppressWarnings("unchecked")
	protected void updateView(Object returnValue) {

		if (returnValue instanceof String)
			view.updateView((String) returnValue);

		else if (returnValue instanceof int[][])
			view.updateView((int[][]) returnValue);

		else if (returnValue instanceof Maze3d)
			view.updateView((Maze3d) returnValue);

		else if (returnValue instanceof Solution)
			view.updateView((Solution<Position>) returnValue);
	}

	@Override
	public synchronized void update(Observable o, Object arg) {
		throw new IllegalAccessError();
	}

	@FunctionalInterface
	protected interface ErrorfulCommand {

		Object run(String[] arguments)
				throws FileNotFoundException, IndexOutOfBoundsException, IllegalArgumentException;
	}
}
