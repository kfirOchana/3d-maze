package presenter.commands;


import model.Model;
import view.View;

public class DisplayMaze extends AbstractInteractiveCommand {

	private static final String KEYWORD = "display";
	private static final String ARGUMENTS = "<name>";

	private static final String HELP = "Prints the maze named <name>.";

	public DisplayMaze(Model model, View view) {
		super(model, view, KEYWORD, ARGUMENTS, HELP);
	}

	@Override
	public void doCommand(String[] arguments) {
		wrapFunctionWithErrorHandler(this::displayMaze, arguments);
	}

	private Object displayMaze(String[] arguments) throws IndexOutOfBoundsException {

		String mazeName = arguments[0];

		return model.getMaze(mazeName);
	}
}
