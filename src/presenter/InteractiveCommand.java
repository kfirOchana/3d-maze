package presenter;

public interface InteractiveCommand extends Command {
    String getHelp();

    String getCallFormat();

    String getKeyword();

    String getArguments();
}
