package view.gui;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.util.Stack;

public class ScreensController extends StackPane {

    Stack<Node> screensStack = new Stack<>();

    public void pushScreen(String resource) {
        screensStack.push(loadScreen(resource));
        setScreen(screensStack.peek());
    }

    public void popScreen() {
        screensStack.pop();
        setScreen(screensStack.peek());
    }

    private Node loadScreen(String resource) {
        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            Parent loadScreen = myLoader.load();
            ControlledScreen myScreenController = myLoader.getController();
            myScreenController.setScreenParent(this);
            return loadScreen;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private boolean setScreen(final Node screen) {

        final DoubleProperty opacity = opacityProperty();

        //Is there is more than one screen
        if (!getChildren().isEmpty()) {
            Timeline fade = new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(opacity, 1.0)),
                    new KeyFrame(new Duration(1000),

                            new EventHandler<ActionEvent>() {

                                @Override
                                public void handle(ActionEvent t) {
                                    //remove displayed screen
                                    getChildren().remove(0);
                                    //add new screen
                                    getChildren().add(0, screen);
                                    Timeline fadeIn = new Timeline(
                                            new KeyFrame(Duration.ZERO,
                                                    new KeyValue(opacity, 0.0)),
                                            new KeyFrame(new Duration(800),
                                                    new KeyValue(opacity, 1.0)));
                                    fadeIn.play();
                                }
                            }, new KeyValue(opacity, 0.0)));
            fade.play();
        } else {
            //no one else been displayed, then just show
            setOpacity(0.0);
            getChildren().add(screen);
            Timeline fadeIn = new Timeline(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(opacity, 0.0)),
                    new KeyFrame(new Duration(2500),
                            new KeyValue(opacity, 1.0)));
            fadeIn.play();
        }
        return true;
    }

    public boolean unloadScreen(String name) {
        if (screens.remove(name) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }
}