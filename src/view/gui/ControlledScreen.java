package view.gui;


public interface ControlledScreen {

    //This method will allow the injection of the Parent ScreenPane
    void setScreenParent(ScreensController screenPage);
}