package view.commandLine;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * This is generic Command-Line-Interface class.
 * @see "https://en.wikipedia.org/wiki/Command-line_interface"
 */
class CLI extends Thread {

	private static final String EMPTY_INPUT = "";
	private static final String EMPTY_ARGUMENT = "";

	private static final char QUOTE_SEPARATOR = '\"';

	private static final String WHITE_SPACE = " ";
	private static final String WHITE_SPACES = "\\s+";

	private final String QUIT_KEYWORD;
	private final BufferedReader in;
	private final PrintWriter out;
	private final BiConsumer<String, String[]> handleCommand;

	CLI(BufferedReader streamIn, PrintWriter streamOut,
	    BiConsumer<String, String[]> handleCommand,
	    String quitKeyword) {

		this.in = streamIn;
		this.out = streamOut;
		this.handleCommand = handleCommand;

		this.QUIT_KEYWORD = quitKeyword;
		// NOTE: Do NOT print the menu from the constructor, <code>keywordToCommandMap</code> may be not initialized.
	}

	/**
	 * Removing redundant white space in the start/end and regulating the separators.
	 *
	 * @param usersInput The input of the user.
	 * @return String in format "CommandKeyword <arg_1> <arg_2> ... <arg_n>" ( or "" if
	 */
	private static String formatInput(String usersInput) {
		return usersInput.toLowerCase().trim();
	}

	/**
	 * Removing the arguments ( if there are any ) and returning the command-keyword.
	 *
	 * @param usersInput The input of the user.
	 * @return The name ( so called keyword ) of the command.
	 * @pre @abst usersInput = "CommandKeyword <arg_1> <arg_2> ... <arg_n>"
	 */
	private static String parseCommandKeyword(String usersInput) {
		try {
			return usersInput.substring(0, usersInput.indexOf(WHITE_SPACE));

			// The specified command doesn't have any arguments i.g it doesn't contain WHITE_SPACE.
		} catch (IndexOutOfBoundsException e) {
			return usersInput;
		}
	}

	/**
	 * Removing the Command-keyword and splitting the arguments into array
	 *
	 * @param usersInput The input of the user.
	 * @return Array of the <code>parseCommandKeyword( usersInput )</code>'s arguments.
	 */
	private static String[] parseCommandArguments(String usersInput) {

		final int argumentsStartIndex;
		if ((argumentsStartIndex = usersInput.indexOf(WHITE_SPACE)) != -1) {

			final List<String> arguments = new ArrayList<>();
			StringBuilder currentArgument = new StringBuilder();
			boolean isInTheMiddleOfAQuote = false;

			for (Character character : usersInput.substring(argumentsStartIndex).toCharArray()) {

				// In case this is a start of a Quote.
				if (character.equals(QUOTE_SEPARATOR)) {
					isInTheMiddleOfAQuote = !isInTheMiddleOfAQuote;
					currentArgument = addArgument(arguments, currentArgument);

					// In case we in the middle of a Quote.
				} else if (isInTheMiddleOfAQuote)
					currentArgument.append(character);

					// In case this is a white space (that separate between two arguments).
				else if (!character.toString().matches(WHITE_SPACES))
					currentArgument.append(character);

					// If we are in the middle of argument.
				else currentArgument = addArgument(arguments, currentArgument);
			}
			addArgument(arguments, currentArgument);

			return convertArgumentsListToArray(arguments);
		} else return new String[0];
	}

	private static String[] convertArgumentsListToArray(List<String> arguments) {
		String[] strings = new String[arguments.size()];
		arguments.toArray(strings);
		return strings;
	}

	private static StringBuilder addArgument(List<String> arguments, StringBuilder currentArgument) {
		if (!currentArgument.toString().equals(EMPTY_ARGUMENT)) {
			arguments.add(currentArgument.toString());
			currentArgument = new StringBuilder();
		}
		return currentArgument;
	}

	@Override
	public void run() {
		String usersInput;

		try {
			do {
				usersInput = formatInput(in.readLine());
				if (!usersInput.equals(EMPTY_INPUT))
					handleCommand.accept(
							parseCommandKeyword(usersInput),
							parseCommandArguments(usersInput));

			} while (!usersInput.equals(QUIT_KEYWORD));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void write(String s) {
		out.println(s);
	}


	void closeAssets() {
		try {
			out.flush();
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
