package view.commandLine;

import javafx.util.Pair;
import maze_generators.Maze3d;
import position.Position;
import search.Solution;
import view.View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Observable;

import static to_string_utils.Array.array2dToString;

public class CommandLineView extends Observable implements View {

	private final CLI cli;

	public CommandLineView(String quitKeyword) {

		this.cli = new CLI(
				new BufferedReader(new InputStreamReader(System.in)),
				new PrintWriter(System.out, true),
				this::letPresenterHandleCommand,
				quitKeyword);
	}

	private void letPresenterHandleCommand(String command, String[] arguments) {
		setChanged();
		notifyObservers(new Pair<>(command, arguments));
	}

	@Override
	public void start() {
		cli.start();
	}

	@Override
	public void close() {
		cli.closeAssets();
	}

	@Override
	public void updateView(String message) {
		cli.write(message);
	}

	@Override
	public void updateView(Maze3d maze3d) {
		cli.write(maze3d.toString());
	}

	@Override
	public void updateView(Solution<Position> mazeSolution) {
		cli.write(mazeSolution.toString());
	}

	@Override
	public void updateView(int[][] mazeCrossSection) {
		cli.write(array2dToString(mazeCrossSection));
	}
}
