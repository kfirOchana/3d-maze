package view;


import maze_generators.Maze3d;
import position.Position;
import search.Solution;

import java.util.Observer;

public interface View {

	void start();

	void close();

	void addObserver(Observer o);

	void updateView(String message);

	void updateView(Maze3d maze3d);

	void updateView(Solution<Position> mazeSolution);

	void updateView(int[][] mazeCrossSection);
}
