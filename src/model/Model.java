package model;

import maze_generators.Maze3d;
import maze_generators.Maze3dGenerator;
import position.Position;
import search.Searcher;
import search.Solution;

import java.io.File;
import java.util.Observer;

public interface Model {

	void deleteObserver(Observer o);

	void addObserver(Observer o);

	void close();


	void generateMaze(String name, int height, int width, int depth, Maze3dGenerator maze3dGenerator, Object finishedNotification);

	void saveMaze(String mazeName, String filePath, Object finishedNotification);

	void loadMaze(String filePath, String mazeName, Object finishedNotification);

	void solveMaze(String mazeName, Searcher<Position> algorithm, Object finishedNotification);


	Solution<Position> getSolution(String mazeName);

	File[] getDirectoryContent(String filePath);

	Maze3d getMaze(String name);

	int[][] getCrossSectionByX(String mazeName, int xPosition) throws IndexOutOfBoundsException;

	int[][] getCrossSectionByY(String mazeName, int yPosition) throws IndexOutOfBoundsException;

	int[][] getCrossSectionByZ(String mazeName, int zPosition) throws IndexOutOfBoundsException;
}
