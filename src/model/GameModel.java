package model;

import boot.ModelProperties;
import io.MyCompressorOutputStream;
import io.MyDecompressorInputStream;
import io.StreamAnalyser;
import maze_generators.Maze3d;
import maze_generators.Maze3dGenerator;
import maze_generators.SearchableMaze3d;
import position.Position;
import search.Searcher;
import search.Solution;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

public class GameModel extends Observable implements Model {

    private static final String TABLE_NAME = "maze_solutions_cache";
    private static final String KEY_COLUMN_NAME = "maze";
    private static final String VALUE_COLUMN_NAME = "solution";

    private static final String SOLUTIONS_CACHING_DISABLED = "Failed to connect to Server, previous Solutions caching won't be loaded.";
    private static final String SOLUTIONS_CACHING_ENABLED = "Successfully connected to server, loaded previous Solutions caching successfully.";
    private static final String ERASE_SOLUTIONS_CACHING = "Failed to connect to Server, Solutions caching erased.";

	private final Map<String, Maze3d> nameToMazeMap = Collections.synchronizedMap(new HashMap<>());
	private final List<Observer> observers = new LinkedList<>();
	private final ExecutorService threadPool;
	private final MapDataBaseManager<Maze3d, Solution<Position>> mapDataBaseManager;
	private Map<Maze3d, Solution<Position>> mazeToSolutionMap;
	private boolean operationWithServerDoneSuccessfully;

	public GameModel(ModelProperties defaultModelProperties) {

		mapDataBaseManager = new MapDataBaseManager<>(
				defaultModelProperties.getDefaultDataBaseURL(),
				defaultModelProperties.getDefaultDataBaseUsername(),
                defaultModelProperties.getDefaultDataBasePassword(),
                TABLE_NAME, KEY_COLUMN_NAME, VALUE_COLUMN_NAME);
        this.threadPool = Executors.newFixedThreadPool(defaultModelProperties.getDefaultThreadNumberLimit());

		try {
			mazeToSolutionMap = readCacheFromDB(mapDataBaseManager);
			operationWithServerDoneSuccessfully = true;

		} catch (SQLException e) {
			mazeToSolutionMap = new HashMap<>();
			operationWithServerDoneSuccessfully = false;
		}
	}

	private static Map<Maze3d, Solution<Position>> readCacheFromDB(
			MapDataBaseManager<Maze3d, Solution<Position>> mapDataBaseManager) throws SQLException {
		//noinspection unchecked
        return mapDataBaseManager.read(
                wrapFuncWithErrorHandling(
                        (compressedMazeAsBytes) -> {
                            byte[] mazeAsBytes = new byte[StreamAnalyser.getDecompressedMazeSize(
                                    new ByteArrayInputStream(compressedMazeAsBytes))];
//                            noinspection ResultOfMethodCallIgnored
							new MyDecompressorInputStream(
									new ByteArrayInputStream(compressedMazeAsBytes)).read(mazeAsBytes);
							return new Maze3d(mazeAsBytes);
                        }),
                wrapFuncWithErrorHandling(
                        (solutionAsBytes) ->
                                (Solution<Position>) MapDataBaseManager.defaultByteToObjectConverter(solutionAsBytes))
        );
    }

	private static void writeCacheToDB(MapDataBaseManager<Maze3d, Solution<Position>> mapDataBaseManager,
									   Map<Maze3d, Solution<Position>> mazeToSolutionMap) throws SQLException {
		mapDataBaseManager.write(
                mazeToSolutionMap,
                wrapFuncWithErrorHandling((maze3d) -> {
                    ByteArrayOutputStream byteStream = new ByteArrayOutputStream(
                            Maze3d.getCompatibleArrayLengthForMazeDimensions(
                                    maze3d.getMazeHeight(),
                                    maze3d.getMazeWidth(),
                                    maze3d.getMazeDepth()));
                    new MyCompressorOutputStream(byteStream).write((maze3d).toByteArray());
                    return byteStream.toByteArray();
                }),

                wrapFuncWithErrorHandling(
                        MapDataBaseManager::defaultObjectToBytesConverter));
    }

    @SuppressWarnings("Duplicates")
	private static <T, R> Function<T, R> wrapFuncWithErrorHandling(ErrorfulFunction<T, R> errorfulFunction) {
		return (T t) -> {
			try {
				return errorfulFunction.apply(t);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		};
	}

    @Override
    public void close() {
		try {
			writeCacheToDB(mapDataBaseManager, mazeToSolutionMap);
			operationWithServerDoneSuccessfully = true;
		} catch (SQLException e) {
			operationWithServerDoneSuccessfully = false;
		}
		observers.forEach(this::deleteObserver);
		threadPool.shutdownNow();
    }

	@Override
	public void generateMaze(String name, int height, int width, int depth,
                             Maze3dGenerator maze3dGenerator, Object finishedNotification) {
        runProcedureInThreadPool(
                () -> nameToMazeMap.put(name, maze3dGenerator.generate(height, width, depth)),
				finishedNotification);
	}

	@Override
	public void saveMaze(String mazeName, String filePath, Object finishedNotification) {
		runProcedureInThreadPool(
				() -> new MyCompressorOutputStream(new FileOutputStream(filePath))
						.write(nameToMazeMap.get(mazeName).toByteArray()),
				finishedNotification);
	}

	@Override
	public void loadMaze(String filePath, String mazeName, Object finishedNotification) {
		runProcedureInThreadPool(() -> {
			byte[] dataFromFile = new byte[StreamAnalyser.getDecompressedMazeSize(new FileInputStream(filePath))];
			//noinspection ResultOfMethodCallIgnored
			new MyDecompressorInputStream(new FileInputStream(filePath)).read(dataFromFile);
			nameToMazeMap.put(mazeName, new Maze3d(dataFromFile));
		}, finishedNotification);
	}

	@Override
	public void solveMaze(String mazeName, Searcher<Position> algorithm, Object finishedNotification) {
        runProcedureInThreadPool(() ->
                        mazeToSolutionMap.computeIfAbsent(nameToMazeMap.get(mazeName), maze -> algorithm.search(new SearchableMaze3d(maze))),
                finishedNotification
        );
    }

	/**
	 * Add {@param procedure} to the thread-pool.
	 * Any thrown Exceptions crashes the procedure and get notified to the Presenter.
	 *
	 * @param procedure            model procedure.
	 * @param finishedNotification Object that notifies the Presenter on the successful completion of the procedure.
	 */
	private void runProcedureInThreadPool(Procedure procedure, Object finishedNotification) {
		threadPool.submit(() -> {
			try {
				procedure.run();
				notifyObservers(finishedNotification);

			} catch (Exception e) {
				notifyObservers(e);
			}
		});
	}

	@Override
	public Solution<Position> getSolution(String mazeName) {
		return mazeToSolutionMap.get(this.nameToMazeMap.get(mazeName));
	}

	@Override
	public int[][] getCrossSectionByX(String mazeName, int xPosition) throws ArrayIndexOutOfBoundsException {
		return nameToMazeMap.get(mazeName).getCrossSectionByX(xPosition);
	}

	@Override
	public int[][] getCrossSectionByY(String mazeName, int yPosition) throws ArrayIndexOutOfBoundsException {
		return nameToMazeMap.get(mazeName).getCrossSectionByY(yPosition);
	}

	@Override
	public int[][] getCrossSectionByZ(String mazeName, int zPosition) throws ArrayIndexOutOfBoundsException {
		return nameToMazeMap.get(mazeName).getCrossSectionByZ(zPosition);
	}

	@Override
	public File[] getDirectoryContent(String path) {
		return new File(path).listFiles();
	}

	@Override
	public Maze3d getMaze(String name) {
		return nameToMazeMap.get(name);
	}

	@Override
	public void notifyObservers() {
		setChanged();
		super.notifyObservers();
	}

	@Override
	public void notifyObservers(Object arg) {
		setChanged();
		super.notifyObservers(arg);
	}

	@Override
	public synchronized void addObserver(Observer o) {
		super.addObserver(o);
		notifyObservers(operationWithServerDoneSuccessfully ?
				SOLUTIONS_CACHING_ENABLED :
				SOLUTIONS_CACHING_DISABLED);
		observers.add(o);
	}

	@Override
	public synchronized void deleteObserver(Observer o) {
		if (!operationWithServerDoneSuccessfully)
			notifyObservers(ERASE_SOLUTIONS_CACHING);

		super.deleteObserver(o);
		observers.remove(o);
	}

	@FunctionalInterface
	private interface ErrorfulFunction<T, R> {
		R apply(T t) throws Exception;
	}

	@FunctionalInterface
	private interface Procedure {

		void run() throws Exception;
	}
}
