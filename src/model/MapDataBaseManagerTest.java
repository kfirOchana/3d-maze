package model;

import io.MyCompressorOutputStream;
import io.MyDecompressorInputStream;
import io.StreamAnalyser;
import maze_generators.GrowingTreeGenerator_LastSection;
import maze_generators.Maze3d;
import maze_generators.SearchableMaze3d;
import org.junit.Before;
import org.junit.Test;
import position.Position;
import search.BestFirstSearch;
import search.Solution;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public class MapDataBaseManagerTest {

    private MapDataBaseManager<Maze3d, Solution<Position>> m;
    private Map<Maze3d, Solution<Position>> mazeToSolutionMap = new HashMap<>();

    @SuppressWarnings("Duplicates")
    private static <T, R> Function<T, R> wrapFuncWithErrorHandling(ErrorfulFunction<T, R> errorfulFunction) {
        return (T t) -> {
            try {
                return errorfulFunction.apply(t);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        };
    }

    @Before
    public void setUp() throws Exception {
        m = new MapDataBaseManager<>("jdbc:mysql://localhost:3306/cache", "root", "", "maze_solutions_cache", "maze", "solution");

        Maze3d maze3d = new GrowingTreeGenerator_LastSection().generate(10, 10, 10);
        mazeToSolutionMap.put(maze3d, new BestFirstSearch<Position>().search(new SearchableMaze3d(maze3d)));
    }

    @Test
    public void read() throws Exception {
        //noinspection unchecked
        mazeToSolutionMap = m.read(
                wrapFuncWithErrorHandling(
                        (compressedMazeAsBytes) -> {
                            byte[] mazeAsBytes = new byte[StreamAnalyser.getDecompressedMazeSize(
                                    new ByteArrayInputStream(compressedMazeAsBytes))];
                            //noinspection ResultOfMethodCallIgnored
                            new MyDecompressorInputStream(new ByteArrayInputStream(compressedMazeAsBytes)).read(mazeAsBytes);
                            return new Maze3d(mazeAsBytes);
                        }),
                wrapFuncWithErrorHandling(
                        (solutionAsBytes) ->
                                (Solution<Position>) MapDataBaseManager.defaultByteToObjectConverter(solutionAsBytes))
        );
        System.out.println(mazeToSolutionMap);
    }

    @Test
    public void write() throws Exception {
        m.write(
                this.mazeToSolutionMap,
                wrapFuncWithErrorHandling((maze3d) -> {
                    ByteArrayOutputStream byteStream = new ByteArrayOutputStream(
                            Maze3d.getCompatibleArrayLengthForMazeDimensions(
                                    maze3d.getMazeHeight(),
                                    maze3d.getMazeWidth(),
                                    maze3d.getMazeDepth()));
                    new MyCompressorOutputStream(byteStream).write((maze3d).toByteArray());
                    return byteStream.toByteArray();
                }),

                wrapFuncWithErrorHandling(
                        MapDataBaseManager::defaultObjectToBytesConverter));
    }

    @FunctionalInterface
    private interface ErrorfulFunction<T, R> {
        R apply(T t) throws Exception;
    }

}