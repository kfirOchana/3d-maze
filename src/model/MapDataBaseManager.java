package model;


import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


class MapDataBaseManager<KeyType, ValueType> {

    private static final String ROW_INSERTION_QUERY =
            "INSERT IGNORE INTO %s (hash_code, %s, %s) VALUES (?, ?, ?)";
    private static final String SELECT_ALL_VALUES_QUERY = "SELECT * FROM %s";
    private final String dataBaseUrl;
    private final String tableName;
    private final String username;
    private final String password;
    private final String keyColumnName;
    private final String valueColumnName;

    MapDataBaseManager(String databaseURL, String userName, String password, String tableName,
                       String keyColumnName, String valueColumnName) {
        this.dataBaseUrl = databaseURL;
        this.username = userName;
        this.password = password;
        this.tableName = tableName;

        this.keyColumnName = keyColumnName;
        this.valueColumnName = valueColumnName;
    }

    static byte[] defaultObjectToBytesConverter(Object o) throws IOException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        objectOutputStream.writeObject(o);
        byte[] keyAsBytes = byteArrayOutputStream.toByteArray();

        objectOutputStream.flush();
        objectOutputStream.close();

        return keyAsBytes;
    }

    static Object defaultByteToObjectConverter(byte[] bytes) throws IOException, ClassNotFoundException {

        ByteArrayInputStream byteArrayInputStream;
        ObjectInputStream objectInputStream;

        byteArrayInputStream = new ByteArrayInputStream(bytes);
        objectInputStream = new ObjectInputStream(byteArrayInputStream);
        return objectInputStream.readObject();
    }

    Map<KeyType, ValueType> read(
            Function<byte[], KeyType> keyExtractionFunc,
            Function<byte[], ValueType> valueExtractionFunc) throws SQLException {

        Map<KeyType, ValueType> mapLoadedFromDB = Collections.synchronizedMap(new HashMap<KeyType, ValueType>());
        ResultSet allDBRows = executeSelectAllValuesQuery();

        while (allDBRows.next()) {

            mapLoadedFromDB.put(
                    keyExtractionFunc.apply(allDBRows.getBytes(this.keyColumnName)),
                    valueExtractionFunc.apply(allDBRows.getBytes(this.valueColumnName)));
        }
        return mapLoadedFromDB;
    }

    private ResultSet executeSelectAllValuesQuery() throws SQLException {
        return getConnectionToDB().prepareStatement(
                String.format(SELECT_ALL_VALUES_QUERY, tableName)).executeQuery();
    }

    void write(
            Map<KeyType, ValueType> map,
            Function<KeyType, byte[]> keyInsertionFunc,
            Function<ValueType, byte[]> valueInsertionFunc) throws SQLException {


        for (KeyType key : map.keySet()) {
            Connection connection = getConnectionToDB();

            PreparedStatement preparedStatement = connection.prepareStatement(
                    String.format(ROW_INSERTION_QUERY, this.tableName, this.keyColumnName, this.valueColumnName));


            byte[] keyAsBytes = keyInsertionFunc.apply(key);
            byte[] valueAsBytes = valueInsertionFunc.apply(map.get(key));

            preparedStatement.setInt(1, Arrays.hashCode(keyAsBytes));
            preparedStatement.setBytes(2, keyAsBytes);
            preparedStatement.setBytes(3, valueAsBytes);

            preparedStatement.executeUpdate();
        }
    }

    private Connection getConnectionToDB() throws SQLException {
        return DriverManager.getConnection(this.dataBaseUrl, this.username, this.password);
    }
}