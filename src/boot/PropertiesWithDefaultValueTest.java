package boot;

import org.junit.Test;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class PropertiesWithDefaultValueTest {

    @Test
    public void get() throws FileNotFoundException {
        XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(Run.SAVED_SETTING_PATH)));
        DefaultProperties defaultProperties = (DefaultProperties) decoder.readObject();
        decoder.close();
        System.out.println(defaultProperties.getDefaultDataBaseURL());
        System.out.println(defaultProperties.getDefaultMazeGenerationAlgorithm());
        System.out.println(defaultProperties.getDefaultMazeSolveAlgorithm());
    }
}