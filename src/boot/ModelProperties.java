package boot;


public interface ModelProperties {


    String getDefaultDataBaseURL();

    String getDefaultDataBaseUsername();

    String getDefaultDataBasePassword();

    int getDefaultThreadNumberLimit();
}
