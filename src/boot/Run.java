package boot;

import model.GameModel;
import model.Model;
import presenter.GamePresenter;
import presenter.InteractiveCommand;
import presenter.commands.*;
import view.View;
import view.commandLine.CommandLineView;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class Run {

	static final String SAVED_SETTING_PATH = "external_files\\properties.xml";

	@SuppressWarnings("unchecked")
    private static final Class<? extends InteractiveCommand>[] commandClasses = new Class[]{
            Exit.class,
            Menu.class,
			DirectoryLookup.class,
			DisplayMaze.class,
			DisplaySolution.class,
			LoadMaze.class,
			MazeCrossSectionDisplay.class,
			MazeGeneration.class,
			SaveMaze.class,
			SolveMaze.class};

	public static void main(String[] args)
			throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, FileNotFoundException {

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(SAVED_SETTING_PATH)));
		DefaultProperties defaultProperties = (DefaultProperties) decoder.readObject();
		decoder.close();


        HashMap<String, InteractiveCommand> keywordToCommandMap = new HashMap<>();

		View view = new CommandLineView(Exit.getExitKeyword());
		Model model = new GameModel(defaultProperties);

		GamePresenter presenter = new GamePresenter(model, view, keywordToCommandMap, defaultProperties);
		view.addObserver(presenter);
		model.addObserver(presenter);

		addCommandsToMap(keywordToCommandMap, view, model);

		view.start();
	}

    private static void addCommandsToMap(HashMap<String, InteractiveCommand> keywordToCommandMap, View view, Model model)
            throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        for (Class<? extends InteractiveCommand> commandClass : commandClasses) {
            InteractiveCommand command = commandClass.getConstructor(Model.class, View.class).newInstance(model, view);
            keywordToCommandMap.put(command.getKeyword(), command);
        }
	}
}
