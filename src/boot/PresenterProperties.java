package boot;


public interface PresenterProperties {


    String getDefaultMazeSolveAlgorithm();

    String getDefaultMazeGenerationAlgorithm();
}
