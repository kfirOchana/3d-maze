package boot;

import javafx.util.Pair;
import presenter.commands.MazeGeneration;
import presenter.commands.SolveMaze;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Properties;

public class DefaultProperties implements Serializable, PresenterProperties, ModelProperties {

    private static final String ILLEGAL_ARGUMENTS_PROMPT =
            String.format("ERROR! Invalid arguments set. Try Input in format: \n'<%s>, <%s>, <%s>, <%s>, <%s>, <%s>'",
                    SolveMaze.getOptionalArguments(), MazeGeneration.getOptionalArguments(),
                    "database_url", "database_username", "database_password", "threads_number_limit");
    private Properties defaultProperties;

    public DefaultProperties() {
    }

    @SafeVarargs
    private DefaultProperties(Pair<PropertiesWithDefaultValue, String>... properties) {
        defaultProperties = new Properties();
        for (Pair<PropertiesWithDefaultValue, String> property : properties) {
            defaultProperties.put(property.getKey(), property.getValue());
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        try {
            updateDefaultPropertiesValue(
                    args[0].toLowerCase(),
                    args[1].toLowerCase(),
                    args[2].toLowerCase(),
                    args[3].toLowerCase(),
                    args[4].toLowerCase(),
                    args[5].toLowerCase());
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(ILLEGAL_ARGUMENTS_PROMPT);
        }
    }

    private static void updateDefaultPropertiesValue(
            String MAZE_SOLVE_ALGORITHM,
            String MAZE_GENERATION_ALGORITHM,
            String DATABASE_URL,
            String DATABASE_USERNAME,
            String DATABASE_PASSWORD,
            String THREADS_NUMBER_LIMIT) throws FileNotFoundException {

        DefaultProperties properties = new DefaultProperties(
                new Pair<>(PropertiesWithDefaultValue.MAZE_SOLVE_ALGORITHM, MAZE_SOLVE_ALGORITHM),
                new Pair<>(PropertiesWithDefaultValue.MAZE_GENERATION_ALGORITHM, MAZE_GENERATION_ALGORITHM),

                new Pair<>(PropertiesWithDefaultValue.DATABASE_URL, DATABASE_URL),
                new Pair<>(PropertiesWithDefaultValue.DATABASE_USERNAME, DATABASE_USERNAME),
                new Pair<>(PropertiesWithDefaultValue.DATABASE_PASSWORD, DATABASE_PASSWORD),

                new Pair<>(PropertiesWithDefaultValue.THREADS_NUMBER_LIMIT, THREADS_NUMBER_LIMIT)
        );

        System.out.println(properties);

        XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(Run.SAVED_SETTING_PATH)));
        encoder.writeObject(properties);
        encoder.close();
    }

    public String getDefaultMazeSolveAlgorithm() {
        return (String) this.defaultProperties.get(PropertiesWithDefaultValue.MAZE_SOLVE_ALGORITHM);
    }

    public String getDefaultMazeGenerationAlgorithm() {
        return (String) this.defaultProperties.get(PropertiesWithDefaultValue.MAZE_GENERATION_ALGORITHM);
    }

    public String getDefaultDataBaseURL() {
        return (String) this.defaultProperties.get(PropertiesWithDefaultValue.DATABASE_URL);
    }

    public String getDefaultDataBaseUsername() {
        return (String) this.defaultProperties.get(PropertiesWithDefaultValue.DATABASE_USERNAME);
    }

    public String getDefaultDataBasePassword() {
        return (String) this.defaultProperties.get(PropertiesWithDefaultValue.DATABASE_PASSWORD);
    }

    public int getDefaultThreadNumberLimit() {
        return Integer.parseInt((String) this.defaultProperties.get(PropertiesWithDefaultValue.THREADS_NUMBER_LIMIT));
    }

    @Override
    public String toString() {
        return defaultProperties.toString();
    }

    public Properties getDefaultProperties() {
        return defaultProperties;
    }

    /**
     * DON'T REMOVE!!! necessary to match JavaBean conventions.
     * @param defaultProperties other value for the default properties.
     */
    public void setDefaultProperties(Properties defaultProperties) {
        this.defaultProperties = defaultProperties;
    }

    private enum PropertiesWithDefaultValue {
        MAZE_SOLVE_ALGORITHM,
        MAZE_GENERATION_ALGORITHM,
        DATABASE_URL,
        DATABASE_USERNAME,
        DATABASE_PASSWORD,
        THREADS_NUMBER_LIMIT
    }
}
