import io.MyCompressorOutputStream;
import io.MyDecompressorInputStream;
import maze_generators.GrowingTreeGenerator_LastSection;
import maze_generators.Maze3d;
import maze_generators.Maze3dGenerator;

import java.io.*;

public class CompressionTest {

    public static void main(String[] args) throws IOException {
        run(new GrowingTreeGenerator_LastSection(), 4, 5, 6);
    }

    private static void run(Maze3dGenerator mg, final int MAZE_HEIGHT, final int MAZE_WIDTH, final int MAZE_DEPTH) throws IOException {

        Maze3d maze = mg.generate(MAZE_HEIGHT, MAZE_WIDTH, MAZE_DEPTH); //... generate it

		// save it to a file
		OutputStream out = new MyCompressorOutputStream(
				new FileOutputStream("1.maz"));
		out.write(maze.toByteArray());
		out.flush();
		out.close();
		InputStream in = new MyDecompressorInputStream(
				new FileInputStream("1.maz"));
		byte b[] = new byte[maze.toByteArray().length];
		//noinspection ResultOfMethodCallIgnored
		in.read(b);
		in.close();
		Maze3d loaded = new Maze3d(b);
		System.out.println(loaded.equals(maze));
	}
}
