import maze_generators.SimpleMaze3dGenerator;
import org.junit.Test;


public class SimpleMaze3dGeneratorTest {

    @Test
    public void generate() throws Exception {
        MazeGeneratorTest.testGenerator(new SimpleMaze3dGenerator(), 4, 5, 6);
    }
}