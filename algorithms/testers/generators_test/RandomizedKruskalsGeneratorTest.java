import maze_generators.Maze3dGenerator;
import maze_generators.RandomizedKruskalsGenerator;
import maze_generators.SearchableMaze3d;
import org.junit.Test;
import position.Position;
import search.BestFirstSearch;
import search.Searcher;


public class RandomizedKruskalsGeneratorTest {
    @Test
    public void generate() throws Exception {
        MazeGeneratorTest.testGenerator(new RandomizedKruskalsGenerator(), 20, 20, 20);
        Maze3dGenerator maze3dGenerator = new RandomizedKruskalsGenerator();
        Searcher<Position> searcher = new BestFirstSearch<>();
        System.out.println(searcher.search(new SearchableMaze3d(maze3dGenerator.generate(4, 5, 6))));
    }

}