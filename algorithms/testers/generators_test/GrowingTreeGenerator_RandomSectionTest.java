import maze_generators.GrowingTreeGenerator_RandomSection;
import org.junit.Test;


public class GrowingTreeGenerator_RandomSectionTest {

    @Test
    public void generate() throws Exception {
        MazeGeneratorTest.testGenerator(new GrowingTreeGenerator_RandomSection(), 4, 5, 6);
    }
}