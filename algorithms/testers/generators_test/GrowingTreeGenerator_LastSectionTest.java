import maze_generators.GrowingTreeGenerator_LastSection;
import org.junit.Test;


public class GrowingTreeGenerator_LastSectionTest {

    @Test
    public void generate() throws Exception {
        MazeGeneratorTest.testGenerator(new GrowingTreeGenerator_LastSection(), 20, 20, 20);
    }
}