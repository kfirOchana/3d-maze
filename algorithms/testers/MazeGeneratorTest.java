import maze_generators.Maze3d;
import maze_generators.Maze3dGenerator;
import position.Position;

import static to_string_utils.Array.array2dToString;

public class MazeGeneratorTest {

	protected static void testGenerator(Maze3dGenerator mg, final int MAZE_HEIGHT, final int MAZE_WIDTH, final int MAZE_DEPTH) {
		// prints the time it takes the algorithm to run
		System.out.println(mg.measureAlgorithmTime(MAZE_HEIGHT, MAZE_WIDTH, MAZE_DEPTH));
		// generate another 3d maze
		Maze3d maze = mg.generate(MAZE_HEIGHT, MAZE_WIDTH, MAZE_DEPTH);
		// get the maze entrance
		Position p = maze.getStartPosition();
		// print the position
		System.out.println(p); // format "{x,y,z}"
		// get all the possible moves from a position
		String[] moves = maze.getPossibleDirectionsByName(p);
		// print the moves
		for (String move : moves)
			System.out.println(move);
		// prints the maze exit position
		System.out.println(maze.getGoalPosition());

        // get 2d cross sections of the 3d maze
        int[][] maze2dx = maze.getCrossSectionByX(2);
		array2dToString(maze2dx);

        int[][] maze2dy = maze.getCrossSectionByY(4);
		array2dToString(maze2dy);

        int[][] maze2dz = maze.getCrossSectionByZ(0);
		array2dToString(maze2dz);

        try {
            // this should throw an exception!
			maze.getCrossSectionByX(-1);

		} catch (IndexOutOfBoundsException e) {
			System.out.println("good!");
		}
	}
}