import maze_generators.GrowingTreeGenerator_LastSection;
import maze_generators.Maze3d;
import maze_generators.SearchableMaze3d;
import org.junit.Before;
import position.Position;
import search.Searcher;

import java.util.List;

public class SearcherTest {

	protected Maze3d maze;

	protected static boolean testSearcher(Searcher<Position> searcher, Maze3d maze) {
		List<Position> victoryRoad = searcher.search(new SearchableMaze3d(maze)).getVictoryRoad();
		Position currentPosition = victoryRoad.get(0);

		if (!currentPosition.equals(maze.getStartPosition())) return false;

		for (Position nextPosition : victoryRoad) {
			if (!maze.isFreeToPass(currentPosition, nextPosition)) return false;
			currentPosition = nextPosition;
		}

		return currentPosition.equals(maze.getGoalPosition());
	}

	@Before
	public void setUp() throws Exception {
		maze = new GrowingTreeGenerator_LastSection().generate(4, 5, 6);
	}

}
