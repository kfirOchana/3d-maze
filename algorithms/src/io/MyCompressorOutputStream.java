package io;


import java.io.IOException;
import java.io.OutputStream;

public class MyCompressorOutputStream extends OutputStream {

	private static final byte UNSIGNED_255_BYTE = -1;

	/**
	 * OutputStream which stream our output through
	 */
	private final OutputStream out;

	public MyCompressorOutputStream(OutputStream outputStream) {
		this.out = outputStream;
	}

	/**
	 * Write the bytes in <code>dataArray</code>
	 *
	 * @param dataArray bytes of data stored in array
	 * @throws IOException If fail to write <code>dataArray</code> into <code>out</code>
	 */
	public void write(byte[] dataArray) throws IOException {
		byte lastDataByte = 1;
		byte count = 0;
		for (byte currentDataByte : dataArray) {
			if (count != UNSIGNED_255_BYTE && lastDataByte == currentDataByte) {
				count++;
			} else {
				if (count != 0) {
					write(count);
					write(lastDataByte);
				}

				lastDataByte = currentDataByte;
				count = 1;
			}
		}
		write(count);
		write(lastDataByte);
	}

	@Override
	public void write(int b) throws IOException {
		this.out.write(b);
	}
}
