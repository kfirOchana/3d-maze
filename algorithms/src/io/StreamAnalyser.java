package io;


import maze_generators.Maze3d;

import java.io.IOException;
import java.io.InputStream;


public class StreamAnalyser {

	private static final int HEIGHT_INDEX = 0;
	private static final int WIDTH_INDEX = 1;
	private static final int DEPTH_INDEX = 2;

	public static int getDecompressedMazeSize(InputStream inputStream) throws IOException {
		byte[] partialData = new byte[DEPTH_INDEX + 1];
		//noinspection ResultOfMethodCallIgnored
		new MyDecompressorInputStream(inputStream).read(partialData);

		return Maze3d.getCompatibleArrayLengthForMazeDimensions(
				partialData[HEIGHT_INDEX], partialData[WIDTH_INDEX], partialData[DEPTH_INDEX]);
	}
}
