package io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class MyDecompressorInputStream extends InputStream {

	private static final byte END_OF_FILE = -1;
	private static final byte UNSIGNED_255_BYTE = -1;

	private final InputStream in;

	public MyDecompressorInputStream(InputStream inputStream) {
		this.in = inputStream;
	}

	@Override
	public int read(byte[] b) throws IOException {

		int i, numberOfRepetition;
		i = 0;
		try {
			while ((numberOfRepetition = read()) != END_OF_FILE) {   // May throw IOException
				numberOfRepetition = numberOfRepetition >= 0 ? numberOfRepetition : UNSIGNED_255_BYTE + numberOfRepetition;
				Arrays.fill(b, i, i + numberOfRepetition, (byte) read()); // My throw IndexOutOfBoundsException
				i += numberOfRepetition;
			}
			// In case <code>b</code> is not big enough to complete the fill operation
		} catch (ArrayIndexOutOfBoundsException ignore) {
		}

		return i;
	}

	@Override
	public int read() throws IOException {
		return in.read();
	}
}
