package search;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * This class contains implementation for the
 * <p>
 * <p>
 * Q.A From Task Document:
 * <p>
 * -What are the Advantages of Depth-First Search over Best-First search?
 * Best-first search (BFS) expands the fewest nodes among all admissible algorithms using the same cost function,
 * but typically requires exponential space.
 * Depth-first search needs space only linear in the maximum search depth, but expands more nodes than BFS.
 * <p>
 * -Why did you choose this implementation for BFS?
 * These implementation is not subject to infinite loops,
 * and by comparing tracks that merged and taking the cheaper we increase the chances to the best solution.
 * <p>
 * <p>
 * Best First Search Pseudo-code:
 * <p>
 * OPEN = [initial state]	// a priority queue of states to be evaluated
 * CLOSED = []	// a set of states already evaluated
 * <p>
 * while OPEN is not empty
 * do
 * 	1. n  dequeue(OPEN) // Remove the best node from OPEN
 * 	2. add(n,CLOSED)	      // so we won’t check n again
 * 	3. If n is the goal state,
 * 		backtrace path to n (through recorded parents) and return path.
 * 	4. Create n's successors.
 * 	5. For each successor s do:
 * 		a. If s is not in CLOSED and s is not in OPEN:
 * 			i.  update that we came to s from n
 * 			ii. add(s,OPEN)
 * 		b. Otherwise, if this new path is better than previous one
 * 			i.  update that we came to s from n
 * 			ii.  If it is not in OPEN add it to OPEN.
 * 			iii. Otherwise, adjust its priority in OPEN
 * done
 * </p>
 */


public class BestFirstSearch<T> implements Searcher<T> {

	/**
	 * Set of all nodes that has already been expended and explored.
	 */
	private final Set<State<T>> exploredNodes = new HashSet<>();

	/**
	 * Priority-queue for the open nodes. these queue is ordered by the cost of the path.
	 */
	private final PriorityQueue<State<T>> openNodes = new PriorityQueue<>(new Comparator<State<T>>() {
		/**
		 * Override the default comparator, so the States with the lower cost will appear first.
		 * @param o1    The first object
		 * @param o2    The second object
		 * @return comparison result between two state<T> objects.
		 */
		@Override
		public int compare(State<T> o1, State<T> o2) {
			// The comparison is made with every single pair, such that truncating double to int won't effect the order.
			return (int) (o1.getCost() - o2.getCost());
		}
	});


	@Override
	public Solution<T> search(Searchable<T> s) {

		this.openNodes.add(s.getStartState());

		while (!this.openNodes.isEmpty()) {

			State<T> currentBestChoice = this.openNodes.remove();
			this.exploredNodes.add(currentBestChoice);

			if (s.getGoalState().equals(currentBestChoice)) {
				return new Solution<>(currentBestChoice);
			}
			exploreNode(s, currentBestChoice);
		}
		return null;
	}

	/**
	 * In these function we will expend the path to all of $currentBestState's successor.
	 *
	 * @param s                Object which we will conduct the search on.
	 * @param currentBestState The state which has the cheapest travel cost at the moment
	 *
	 *
	 */
	private void exploreNode(Searchable<T> s, State<T> currentBestState) {

		// Expanding the node
		for (State<T> successor : s.getAllPossibleStates(currentBestState)) {
			successor.setOrigin(currentBestState);
			// If the successor is already present, no changes will be applied.
			openNodes.add(successor);
		}
		openNodes.removeAll(exploredNodes);
//		exploredNodes.forEach(openNodes::remove);
	}

	@Override
	public int getNumberOfNodesEvaluated() {
		return this.exploredNodes.size();
	}
}
