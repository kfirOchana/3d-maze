package search;


import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Solution<T> implements Serializable {

	private static final String TO_STRING_SEPARATOR = " -> ";

	/**
	 * The path with the cheapest cost from the given start to the end.
     * NOTE: LinkedList<T> as static type is necessary (instead of List<T>) because of its serializable characteristics
     */
    private final LinkedList<T> victoryRoad = new LinkedList<>();

	Solution(State<T> goalState) {

        State<T> currentState = goalState;
        while (currentState != null) {
			victoryRoad.add(0, currentState.getState());
			currentState = currentState.getOrigin();
		}
	}

	/**
	 * Getter for the solution path
	 *
	 * @return The path that solve the {@link Searchable} problem.
	 */
	public List<T> getVictoryRoad() {
		return victoryRoad;
	}

	@Override
	public String toString() {

		StringBuilder stringBuilder = new StringBuilder();
		Iterator<T> victoryRoadIterator = victoryRoad.iterator();

		while (victoryRoadIterator.hasNext()) {
			stringBuilder.append(victoryRoadIterator.next());

			if (victoryRoadIterator.hasNext())
				stringBuilder.append(TO_STRING_SEPARATOR);
		}

		return stringBuilder.toString();
	}
}
