package search;


import java.util.HashSet;
import java.util.Set;


/**
 * Depth-first Search with Heuristic selection
 *
 * @param <T>   The type of Objects that we search in.
 */
public class DepthFirstSearch<T> implements Searcher<T> {
	/**
	 * Set of all Nodes that we have already Reached.
	 */
	private final Set<State<T>> visitedNodes = new HashSet<>();

	@Override
	public Solution<T> search(Searchable<T> s) {

		State<T> currentLeadingState = s.getStartState();
		this.visitedNodes.add(currentLeadingState);

		while (currentLeadingState != null) {

			if (currentLeadingState.equals(s.getGoalState())) {
				return new Solution<>(currentLeadingState);
			}

			Set<State<T>> unvisitedNeighbors = new HashSet<>(s.getAllPossibleStates(currentLeadingState));
			unvisitedNeighbors.removeAll(visitedNodes); // Removing all the visited neighbors.

			if (unvisitedNeighbors.isEmpty()) { // If we tackled with Dead-End...
				currentLeadingState = currentLeadingState.getOrigin();  // We will pop up the stack

			} else {
				State<T> lowestCostNode = getElementWithTheLowestCost(unvisitedNeighbors);
				this.visitedNodes.add(lowestCostNode);
				lowestCostNode.setOrigin(currentLeadingState);
				currentLeadingState = lowestCostNode;
			}
		}
		return null;
	}

	/**
	 * @param statesSet Set of different states<T>.
	 * @return The State<T> with the lowest cost.
	 */
	private State<T> getElementWithTheLowestCost(Set<State<T>> statesSet) {
		State<T> currentBestState = null;
		for (State<T> state : statesSet) {

			if (currentBestState == null) {
				currentBestState = state;

			} else if (state.getCost() < currentBestState.getCost()) {
				currentBestState = state;
			}
		}
		return currentBestState;
	}


	@Override
	public int getNumberOfNodesEvaluated() {
		return this.visitedNodes.size();
	}
}
