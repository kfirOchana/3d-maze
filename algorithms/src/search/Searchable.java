package search;


import java.util.List;

public interface Searchable<T> {

	/**
	 * Getter for the start state. we will initially expend our search from him
	 *
	 * @return The track's starting point
	 */
	State<T> getStartState();

	/**
	 * Getter for the final state in the track.
	 *
	 * @return The track's ending point.
	 */
	State<T> getGoalState();

	/**
	 * These function will checks the available following states and gathers them into a List.
	 *
	 * @param s The current state.
	 * @return List of all available states.
	 */
	List<State<T>> getAllPossibleStates(State<T> s);
}
