package search;


public class State<T> {
	/**
	 * The state represented by a Generic type
	 */
	private final T state;

	/**
	 * Cost to reach this state from $this.origin
	 */
	private final double cost;

	/**
	 * The state we came from to this state
	 */
	private State<T> origin;

	/**
	 * Constructor that mostly intended to the first State of the Graph, which has no cost.
	 *
	 * @param state State from the {@link Searchable} problem
	 */
	public State(T state) {
		this(state, 0);
	}

	/**
	 * Testers.Main constructor for initializing a state and the cost of transition to him
	 *
	 * @param state State from the {@link Searchable} problem
	 * @param cost  The cost of the transition to him from $this.origin
	 */
	public State(T state, double cost) {
		this.state = state;
		this.cost = cost;
	}


	/**
	 * @return The current T-type state.
	 */
	public T getState() {
		return state;
	}

	/**
	 * @return The cost of moving to this State from getCost()
	 */
	double getCost() {
		return this.cost + (origin == null ? 0 : getOrigin().cost);
	}

	/**
	 * @return The State which we came from.
	 */
	State<T> getOrigin() {
		return origin;
	}

	/**
	 * @param originState The states which we came from.
	 */
	void setOrigin(State<T> originState) {
		this.origin = originState;
	}

	@Override
	public String toString() {
		return this.state.toString();
	}

	@Override
	public boolean equals(Object obj) { // we override Object's equals method
		return obj instanceof State && state.equals(((State) obj).state);
	}

	@Override
	public int hashCode() {
		return state.hashCode();
	}
}