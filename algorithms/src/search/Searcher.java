package search;

public interface Searcher<T> {

	/**
	 * @param s Object which we will conduct the search on.
	 * @return The shortest path from $s.getStartState() to $s.getGoalState()
	 * (Its may NOT be the absolute shortest path, but a close one).
	 * @post $ret == null   if these is not such a path that solve the {@link Searchable} problem
	 */
	Solution<T> search(Searchable<T> s);

	/**
	 * @return The number nodes that were expanded during the search process.
	 */
	int getNumberOfNodesEvaluated();
}
