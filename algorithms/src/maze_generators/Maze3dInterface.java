package maze_generators;

import position.Position;

import java.io.Serializable;

public interface Maze3dInterface extends Serializable {

	/**
	 * @return The maze's height (real-scale)
	 */
	int getMazeHeight();

	/**
	 * @return The maze's width (real-scale)
	 */
	int getMazeWidth();

	/**
	 * @return The maze's depth (real-scale)
	 */
	int getMazeDepth();

	/**
	 * @return The maze's entry position (real-scale).
	 */
	Position getStartPosition();

	/**
	 * @return The maze's finish position (real-scale).
	 */
	Position getGoalPosition();

	/**
	 * @param currentPosition The users's current (real-scale) position in the maze.
	 * @return Array contains all the possible moves (its subset of allDirections.keySet()).
	 */
	String[] getPossibleDirectionsByName(Position currentPosition);

    boolean isFreeToPass(Position from, Position to);
}
