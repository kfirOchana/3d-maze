package maze_generators;

import position.Position;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Maze3d implements Maze3dInterface {
    // NOTE: I refer to 1x1x1 cube from the Maze as Section. Section can be either Blocked (1) or clear-to-pass (0)

	static final int PASS = 0;

	@SuppressWarnings("WeakerAccess")
	static final int BLOCK = 1;

	private static final int START_AND_GOAL_POSITIONS = 6;

	private static final int NUM_OF_DIMENSIONS = 3;

	/**
	 * The 1st dimension in position object will be the mazeHeight
	 */
	private static final int HEIGHT_DIMENSION = 0;

	/**
	 * The 2nd dimension in position object will be the mazeHeight
	 */
	private static final int WIDTH_DIMENSION = 1;

	/**
	 * The 3rd dimension in position object will be the mazeHeight
	 */
	private static final int DEPTH_DIMENSION = 2;

	/**
	 * We represent this maze as a grid where every two blocks has a Barrier (or a Pass) between theme
	 */
	private static final int DISTANCE_BETWEEN_BLOCK = 2;

	private static final String TO_STRING_FORMAT =
			"Dimensions: %d (height) X %d (width) X %d (depth)\n" +
					"Start position: %s\n" +
					"Goal position: %s\n" +
					"Content:\n%s";

	/**
	 * Map from Positions in array-scaled to String that represent that direction (such as "Left", "Up" etc.)
	 */
	private static final Map<Position, String> allDirections = initializePossibleDirectionsMap();
	/**
	 * Those properties will save the maze dimensions in array-scaled for easy access
	 * (to avoid checking $arrayStructure sizes each time).
	 */
	private final int mazeHeight;
	private final int mazeWidth;
	private final int mazeDepth;
	/**
	 * This property present the structure of the 3D-maze.
	 * while $arrayStructure[0][0][0] is the far-lower-left corner,
	 * The $arrayStructure[X][Y][Z] is the Maze section at mazeHeight X, Y steps to the right & Z steps towards the front.
	 * $arrayStructure is initialized with default boolean values ( i.e false ), which means the entire Maze blocked.
	 */
	private final int[][][] arrayStructure;
	/**
	 * The maze's entry position (real-scaled).
	 */
	private Position startPosition;
	/**
	 * The maze's finish position (real-scaled).
	 */
	private Position goalPosition;

	/**
	 * @pre 0 < $mazeHeight, $mazeWidth, $mazeDepth < Integer.MAX_VALUE
	 *
	 * @param height    The mazeHeight of the 3D-MAze
	 * @param width     The mazeWidth of the 3D-Maze
	 * @param depth     The mazeDepth of the 3D-Maze
	 *
	 * @throws java.lang.OutOfMemoryError if the maze is too big for the computer to handle.
	 */
	public Maze3d(int height, int width, int depth) {

		this.startPosition = randomPoint(height, width, depth);
		this.goalPosition = randomPoint(height, width, depth);

		height = convertRealSizeToCompatibleArraySize(height);
		width = convertRealSizeToCompatibleArraySize(width);
		depth = convertRealSizeToCompatibleArraySize(depth);

		this.arrayStructure = createBlocked3dMaze(height, width, depth);

		this.mazeHeight = height;
		this.mazeWidth = width;
		this.mazeDepth = depth;
	}

	/**
	 * @param mazeAsByteArray maze converted to bytes array in the abstract format described above.
	 * @pre $abst $mazeAsByteArray = [start position (3 bytes), goal position (3 bytes), dimensions (3 bytes), content...]
	 * @see Maze3d#toByteArray  That convert maze into byte[]
	 */
	public Maze3d(byte[] mazeAsByteArray) {
		this(mazeAsByteArray[HEIGHT_DIMENSION], mazeAsByteArray[WIDTH_DIMENSION], mazeAsByteArray[DEPTH_DIMENSION]);

		// Setting the maze's start position
		setStartPosition(new Position(
				castBytesToInts(Arrays.copyOfRange(mazeAsByteArray, NUM_OF_DIMENSIONS, 2 * NUM_OF_DIMENSIONS))));

		// Setting the maze's goal position
		setGoalPosition(new Position(
				castBytesToInts(Arrays.copyOfRange(mazeAsByteArray, 2 * NUM_OF_DIMENSIONS, 3 * NUM_OF_DIMENSIONS))));

		// Setting the maze's content.
		for (int i = 0; i < mazeHeight * mazeWidth * mazeDepth; i++) {
			setBlockStatus(
					new Position((i / mazeDepth / mazeWidth) % mazeHeight, (i / mazeDepth) % mazeWidth, i % mazeDepth),
					mazeAsByteArray[i + 3 * NUM_OF_DIMENSIONS]);
		}
	}

	/**
	 * This function creates an $mazeHeight X $mazeWidth X $mazeDepth maze,
	 * initialized with every section surrounded with blocks every single side.
	 * and clear sections so its pattern will remind chess board (each pass will be blocked in all 6 directions).
	 * RETURN EXAMPLE (1 X 1 X 1 board):                     F|F|F
	 *                                               F|F|F / F|F|F
	 *                                       F|F|F / F|T|F / F|F|F
	 *                                       F|F|F / F|F|F /
	 *                                       F|F|T /
	 */
	private static int[][][] createBlocked3dMaze(final int mazeHeight, final int mazeWidth, final int mazeDepth) {
		int[][][] maze3d = new int[mazeHeight][mazeWidth][mazeDepth];

		for (int x = 0; x < mazeHeight; x++) {
			for (int y = 0; y < mazeWidth; y++) {
				for (int z = 0; z < mazeDepth; z++) {
					if (!isValidStandPosition(x, y, z, mazeHeight, mazeWidth, mazeDepth))
						maze3d[x][y][z] = BLOCK;
				}
			}
		}
		return maze3d;
	}

	/**
	 * @param x          height coordinate (array-scale)
	 * @param y          width coordinate (array-scale)
	 * @param z          depth coordinate (array-scale)
	 * @param mazeHeight maze's height (array-scale)
	 * @param mazeWidth  maze's width (array-scale)
	 * @param mazeDepth  maze's depth (array-scale)
	 * @return boolean value that determine whether the coordinate <x, y, z> is within the maze,
	 * and it represent stand position
	 */
	private static boolean isValidStandPosition(int x, int y, int z,
	                                            int mazeHeight, int mazeWidth, int mazeDepth) {
		return ((0 <= x) && (x < mazeHeight)) &&
				((0 <= y) && (y < mazeWidth)) &&
				((0 <= z) && (z < mazeDepth)) &&
				x % 2 + y % 2 + z%2 == 3;
	}

	private static Map<Position, String> initializePossibleDirectionsMap() {
		Map<Position, String> possibleDirections = new HashMap<>();
		possibleDirections.put(new Position(1, 0, 0), "Up");
		possibleDirections.put(new Position(-1, 0, 0), "Down");
		possibleDirections.put(new Position(0, 1, 0), "Right");
		possibleDirections.put(new Position(0, -1, 0), "Left");
		possibleDirections.put(new Position(0, 0, 1), "Forward");
		possibleDirections.put(new Position(0, 0, -1), "Backward");
		return possibleDirections;
	}

	private static Position randomPoint(final int height, final int width, final int depth) {
		Random randomNumbersGenerator = new Random();
		return new Position(
				randomNumbersGenerator.nextInt(height),
				randomNumbersGenerator.nextInt(width),
				randomNumbersGenerator.nextInt(depth));
	}

	/**
	 * @param p1 The first (real-scaled) position
	 * @param p2 The second (real-scaled) position
	 * @return The block between the two positions.
	 * @pre $oldPosition & $newPosition are neighbors!
	 */
    static Position findWallBetweenPositions(Position p1, Position p2) {
        return Position.binaryOperator(
                convertRealScalePositionToArrayScalePosition(p1),
				convertRealScalePositionToArrayScalePosition(p2), Maze3d::average);
	}

    static Position[] findPositionDevidedByWall(Position blockPosition) {
        return new Position[]{
                convertArrayScalePositionToRealScalePosition(Position.unaryOperator(blockPosition, i -> ++i)),
                convertArrayScalePositionToRealScalePosition(Position.unaryOperator(blockPosition, i -> --i))};
    }

	/**
	 * Conventional average Function between two integers.
	 *
	 * @param arg1 First integer
	 * @param arg2 The second integer
	 * @return The average (int!) value of $arg1 & $arg2
	 */
	private static int average(int arg1, int arg2) {
		return (arg1 + arg2) / 2;
	}

	private static Position convertRealScalePositionToArrayScalePosition(Position realScalePosition) {
        return Position.unaryOperator(realScalePosition, Maze3d::convertRealSizeToCompatibleArraySize);
    }

	private static Position convertArrayScalePositionToRealScalePosition(Position realScalePosition) {
        return Position.unaryOperator(realScalePosition, Maze3d::convertArraySizeToRealSize);
    }


	private static int convertRealSizeToCompatibleArraySize(int realSize) {
		return (realSize * DISTANCE_BETWEEN_BLOCK) + 1;
	}

	private static int convertArraySizeToRealSize(int arraySize) {
		return (arraySize - 1) / DISTANCE_BETWEEN_BLOCK;
	}

	private static byte[] castIntsToBytes(int[] ints) {
		byte[] bytes = new byte[ints.length];
		for (int i = 0; i < ints.length; i++) {
			bytes[i] = (byte) ints[i];
		}
		return bytes;
	}

	private static int[] castBytesToInts(byte[] bytes) {
		int[] ints = new int[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			ints[i] = bytes[i];
		}
		return ints;
	}

	public static int getCompatibleArrayLengthForMazeDimensions(int height, int width, int depth) {

		return START_AND_GOAL_POSITIONS +
				NUM_OF_DIMENSIONS +
				convertRealSizeToCompatibleArraySize(height) *
						convertRealSizeToCompatibleArraySize(width) *
						convertRealSizeToCompatibleArraySize(depth);
	}

	/**
	 * @param potentialStandPosition real-scaled position.Position.
	 * @return A boolean value that determines whether $somePosition is within the Maze's boundaries.
	 */
	private boolean isValidStandPosition(Position potentialStandPosition) {
		int x = potentialStandPosition.getLocationAtDimension(HEIGHT_DIMENSION);
		int y = potentialStandPosition.getLocationAtDimension(WIDTH_DIMENSION);
		int z = potentialStandPosition.getLocationAtDimension(DEPTH_DIMENSION);
		return ((0 <= x) && (x < getMazeHeight())) &&
				((0 <= y) && (y < getMazeWidth())) &&
				((0 <= z) && (z < getMazeDepth()));
	}

	/**
	 * This function Blocks or Frees this.arrayStructure[heightIndex][widthIndex][depthIndex]'s section.
	 *
	 * @param sectionCoordinates Is array-scaled position.Position object containing the the following data relatively to this.arrayStructure[0][0][0].
	 *  x - The mazeHeight of the section (number of steps upwards)
	 *  y - The mazeWidth of the section (number of steps to the right)
	 *  z - The mazeDepth of the section (number of steps towards the front)
	 *  (i.q. to this.arrayStructure[x][y][z])
	 *
	 * @param isFreeToPass  boolean value that determine whether the user can pass through this section.
	 *                      optional values are <code>BLOCK</code> ot <code>PASS</code>
	 *
	 * @throws IndexOutOfBoundsException if one of the following conditions is false:
	 *                                   0 <= x < Maze's mazeHeight, 0 <= y < Maze's mazeWidth, 0 <= z < Maze's mazeDepth
	 *
	 * @see this.mazeStructure
	 */
	void setBlockStatus(Position sectionCoordinates, final int isFreeToPass) {
		this.arrayStructure[sectionCoordinates.getLocationAtDimension(HEIGHT_DIMENSION)]
				[sectionCoordinates.getLocationAtDimension(WIDTH_DIMENSION)]
				[sectionCoordinates.getLocationAtDimension(DEPTH_DIMENSION)] = isFreeToPass;
	}

	/**
	 * @param sectionCoordinates Is array-scaled position.Position object containing the the following data relatively to this.arrayStructure[0][0][0].
	 *  x - The mazeHeight of the section (number of steps upwards)
	 *  y - The mazeWidth of the section (number of steps to the right)
	 *  z - The mazeDepth of the section (number of steps towards the front)
	 *  (i.q. to this.arrayStructure[x][y][z])
	 *
	 * @return boolean value that determine whether the Maze section at mazeHeight $heightIndex,
	 *         $widthIndex steps to the right & $depthIndex steps towards the front is not blocked,
	 *         and the user can free to move there.
	 *
	 * @see this.mazeStructure
	 */
	private int getBlockStatus(Position sectionCoordinates) {
		return this.arrayStructure[sectionCoordinates.getLocationAtDimension(HEIGHT_DIMENSION)]
				[sectionCoordinates.getLocationAtDimension(WIDTH_DIMENSION)]
				[sectionCoordinates.getLocationAtDimension(DEPTH_DIMENSION)];
	}

	private List<Position> filterPossibleMoves(Position currentPosition, Predicate<Position> positionPredicate) {
		if (!isValidStandPosition(currentPosition)) throw new IndexOutOfBoundsException();

		List<Position> clearNeighborsPosition = new ArrayList<>();

		Position newPosition;
		for (Position direction : Maze3d.allDirections.keySet()) {
			newPosition = Position.add(currentPosition, direction);
			if (positionPredicate.test(newPosition)) {   // Filtering the new positions
				clearNeighborsPosition.add(direction);
			}
		}

		return clearNeighborsPosition;
	}

	/**
	 * Note: Neighbor need to be [above/under/in front/behind/right to/left to] our $currentPosition
	 * and inside the maze's boundaries
	 *
	 * @param currentPosition The user's current position in the maze.
	 * @return List of directions toward neighbors.
	 * @throws IndexOutOfBoundsException If $currentPosition is not within the maze boundaries
	 * @post $maze3d.isValidStandPosition(pos)   for all pos in $ret
	 */
	private List<Position> getLegalDirections(Position currentPosition) {
		return filterPossibleMoves(currentPosition, this::isValidStandPosition);
	}

	/**
	 * @param currentPosition The user's current position in the maze (real-scale)
	 * @return List $currentPosition's neighbors
	 */
	List<Position> getLegalMoves(Position currentPosition) {
		return getLegalDirections(currentPosition).stream().map(legalDirection ->
				Position.add(currentPosition, legalDirection)).collect(Collectors.toList());
	}

	/**
	 * @param currentPosition The users current location in the Maze (real-scaled position.Position).
	 * @return A sub-list of allDirections.keySet(), s.t every direction in it is free to pass
	 * (there is no barrier in that direction)
	 */
	private List<Position> getFreeOfBriersDirections(Position currentPosition) {
		return getLegalDirections(currentPosition).stream().filter(
				legalDirection ->
                        getBlockStatus(findWallBetweenPositions(
                                currentPosition, Position.add(currentPosition, legalDirection))) == PASS)
				.collect(Collectors.toList());
	}

	/**
	 * @param currentPosition The users current location in the Maze (real-scaled position.Position).
	 * @return An array of all the (real-scaled) Positions that we can move to.
	 */
	Position[] getPossibleMovesByPositions(Position currentPosition) {
		List<Position> possibleDirections = getFreeOfBriersDirections(currentPosition);
		Position[] possibleMoves = new Position[possibleDirections.size()];

		for (int i = 0; i < possibleDirections.size(); i++) {
			possibleMoves[i] = Position.add(currentPosition, possibleDirections.get(i));
		}
		return possibleMoves;
	}

	/**
	 * @param currentPosition The users's current position in the maze.
	 * @return An array of all directions (Strings such as "Left", "Up" etc.) that we can go to.
	 */
	@Override
	public String[] getPossibleDirectionsByName(Position currentPosition) {

		List<Position> possibleMovePositions = getFreeOfBriersDirections(currentPosition);
		String[] possibleMoves = new String[possibleMovePositions.size()];

		for (int i = 0; i < possibleMovePositions.size(); i++) {
			possibleMoves[i] = allDirections.get(possibleMovePositions.get(i));
		}
		return possibleMoves;
	}

	public boolean isFreeToPass(Position from, Position to) {
		from = convertRealScalePositionToArrayScalePosition(from);
		to = convertRealScalePositionToArrayScalePosition(to);
        return getBlockStatus(Position.binaryOperator(from, to, (i1, i2) -> (i1 + i2) / 2)) == PASS;
    }

	/**
	 * @param xPosition real-scaled position.Position
	 * @return 2-Dimensions array of the Plane that shaped when you fix firmly the height dimension to $xPosition
	 */
	public int[][] getCrossSectionByX(int xPosition) {
		xPosition = convertRealSizeToCompatibleArraySize(xPosition);

		return this.arrayStructure[xPosition].clone();
	}

	/**
	 * @param yPosition real-scaled position.Position
	 * @return 2-Dimensions array of the Plane that shaped when you fix firmly the width dimension to $yPosition
	 */
	public int[][] getCrossSectionByY(int yPosition) {
		yPosition = convertRealSizeToCompatibleArraySize(yPosition);

		int[][] crossSectionByY = new int[mazeHeight][mazeDepth];
		for (int x = 0; x < mazeHeight; x++) {
			System.arraycopy(this.arrayStructure[x][yPosition], 0, crossSectionByY[x], 0, mazeDepth);

		}
		return crossSectionByY;
	}

	/**
	 * @param zPosition real-scaled position.Position
	 * @return 2-Dimensions array of the Plane that shaped when you fix firmly the depth dimension to $zPosition
	 */
	public int[][] getCrossSectionByZ(int zPosition) {
		zPosition = convertRealSizeToCompatibleArraySize(zPosition);

		int[][] crossSectionByZ = new int[mazeHeight][mazeWidth];
		for (int x = 0; x < mazeHeight; x++) {
			for (int y = 0; y < mazeWidth; y++) {
				crossSectionByZ[x][y] = this.arrayStructure[x][y][zPosition];
			}
		}
		return crossSectionByZ;
	}

	@Override
	public int getMazeHeight() {
		return convertArraySizeToRealSize(mazeHeight);
	}

	@Override
	public int getMazeWidth() {
		return convertArraySizeToRealSize(mazeWidth);
	}

	@Override
	public int getMazeDepth() {
		return convertArraySizeToRealSize(mazeDepth);
	}

	@Override
	public Position getStartPosition() {
		return startPosition;
	}

	/**
	 * @param startPosition The new value of $this.startPosition (real-scaled position.Position)
	 */
	@SuppressWarnings("WeakerAccess")
	void setStartPosition(Position startPosition) {
		this.startPosition = startPosition;
	}

	@Override
	public Position getGoalPosition() {
		return goalPosition;
	}

	/**
	 * @param goalPosition The new value of $this.goalPosition (real-scaled position.Position)
	 */
	@SuppressWarnings("WeakerAccess")
	void setGoalPosition(Position goalPosition) {
		this.goalPosition = goalPosition;
	}

	/**
	 * @return The maze structure in byte array.
	 * @post $abst $ret = [start position (3 bytes), goal position (3 bytes), dimensions (3 bytes), content...]
	 */
	public byte[] toByteArray() {
		byte[] mazeAsByteArray = new byte[
				getCompatibleArrayLengthForMazeDimensions(getMazeHeight(), getMazeWidth(), getMazeDepth())];

		mazeAsByteArray[HEIGHT_DIMENSION] = (byte) getMazeHeight();
		mazeAsByteArray[WIDTH_DIMENSION] = (byte) getMazeWidth();
		mazeAsByteArray[DEPTH_DIMENSION] = (byte) getMazeDepth();

		System.arraycopy(castIntsToBytes(this.startPosition.toArray()), 0,
				mazeAsByteArray, NUM_OF_DIMENSIONS, NUM_OF_DIMENSIONS);

		System.arraycopy(castIntsToBytes(this.goalPosition.toArray()), 0,
				mazeAsByteArray, 2 * NUM_OF_DIMENSIONS, NUM_OF_DIMENSIONS);

		for (int x = 0; x < mazeHeight; x++) {
			for (int y = 0; y < mazeWidth; y++) {
				for (int z = 0; z < mazeDepth; z++) {
					mazeAsByteArray[
							START_AND_GOAL_POSITIONS +
									NUM_OF_DIMENSIONS +
									x * mazeWidth * mazeDepth + y * mazeDepth + z] = (byte) this.arrayStructure[x][y][z];
				}
			}
		}
		return mazeAsByteArray;
	}

	@Override
	public String toString() {
		return String.format(TO_STRING_FORMAT,
				getMazeHeight(), getMazeWidth(), getMazeDepth(),
				startPosition.toString(), goalPosition.toString(),
				to_string_utils.Array.array3dToString(this.arrayStructure));
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Maze3d && Arrays.deepEquals(this.arrayStructure, ((Maze3d) obj).arrayStructure);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(this.arrayStructure);
    }
}
