package maze_generators;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


abstract class AbstractMaze3dGenerator implements Maze3dGenerator {


	static final Random randomNumbersGenerator = new Random();


	/**
	 * In these format, measureAlgorithmTime will prompt the user on the algorithm execution time.
	 *
	 * OUTPUT EXAMPLE:  "The algorithm generated 20 X 20 X 20 Maze in 43.286 seconds.
	 */
	private static final String ELAPSED_PRINT_FORMAT =
			"The algorithm generated %d X %d X %d Maze in %s seconds";

	private static final String TIME_FORMAT = "ss.SSS";

	/**
	 * @param timeInMillis number of elapsed milliseconds
	 * @return The $ELAPSED_PRINT_FORMAT String with "mm" replaced with the number of minutes,
	 * "ss" replaced with the number of seconds & "SSS" replaced with  the number of milliseconds.
	 */
	private static String convertMillisToPrintFormat(long timeInMillis) {
		return new SimpleDateFormat(TIME_FORMAT).format(new Date(timeInMillis));
	}

	@Override
	public final String measureAlgorithmTime(int mazeHeight, int mazeWidth, int mazeDepth) {
		long startTime = System.currentTimeMillis();    // Saving the current time

		generate(mazeHeight, mazeWidth, mazeDepth); // Running the algorithm

		// Calling convertMillisToPrintFormat with the elapsed time in milliseconds
		// and Formatting its return with the Maze dimension
		return String.format(ELAPSED_PRINT_FORMAT,
				mazeHeight, mazeWidth, mazeDepth,
				convertMillisToPrintFormat(System.currentTimeMillis() - startTime));
	}
}
