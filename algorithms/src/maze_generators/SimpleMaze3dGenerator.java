package maze_generators;


import position.Position;

import java.util.*;

public class SimpleMaze3dGenerator extends AbstractMaze3dGenerator {

    private Maze3d maze3d;
    private Set<Position> visitedPosition;
    private int pathLength;

	private static <T> T getRandomElemFromList(List<T> elements) {
		return elements.get(new Random().nextInt(elements.size()));
	}

	@Override
	public Maze3d generate(int mazeHeight, int mazeWidth, int mazeDepth) {
        pathLength = (int) Math.sqrt(mazeHeight * mazeWidth * mazeDepth);
        maze3d = new Maze3d(mazeHeight, mazeWidth, mazeDepth);
        visitedPosition = new HashSet<>();

        setRandomBlock(maze3d.getStartPosition());
        setValidPath(maze3d.getStartPosition());
        return maze3d;
	}

    private void setValidPath(Position currentPosition) {
        visitedPosition.add(currentPosition);

		if (visitedPosition.size() == pathLength) {
			maze3d.setGoalPosition(currentPosition);
			return;
		}
		List<Position> neighbors = maze3d.getLegalMoves(currentPosition);
		neighbors.removeAll(visitedPosition);
		Position nextPosition = getRandomElemFromList(neighbors);
        maze3d.setBlockStatus(Maze3d.findWallBetweenPositions(currentPosition, nextPosition), Maze3d.PASS);

        setValidPath(nextPosition);
    }

    private void setRandomBlock(Position currentPosition) {
        visitedPosition.add(currentPosition);
        maze3d.getLegalMoves(currentPosition).stream()
                .filter(neighbor -> !visitedPosition.contains(neighbor)).forEach(neighbor -> {
            maze3d.setBlockStatus(Maze3d.findWallBetweenPositions(currentPosition, neighbor),
                    getRandomElemFromList(Arrays.asList(Maze3d.PASS, Maze3d.BLOCK)));
            setRandomBlock(neighbor);
        });
	}
}
