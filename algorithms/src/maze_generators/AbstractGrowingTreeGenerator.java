package maze_generators;

import position.Position;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This is a maze generator, that use the Growing-Tree algorithm to sketch the structure.
 */
abstract class AbstractGrowingTreeGenerator extends AbstractMaze3dGenerator {

	@Override
	public final Maze3d generate(int mazeHeight, int mazeWidth, int mazeDepth) {

		LinkedList<Position> positionLinkedList = new LinkedList<>();
		// This property will save the all the visited positions
		Set<Position> visitedPositions = new HashSet<>();

		Maze3d maze3d = new Maze3d(mazeHeight, mazeWidth, mazeDepth);
		positionLinkedList.push(maze3d.getStartPosition());
		visitedPositions.add(maze3d.getStartPosition());

		while (!positionLinkedList.isEmpty()) {
			// Here we choose either Randomly or Regularly the last element in the queue.
			int positionsIndexToContinueTrackFrom = getPositionsIndexToContinueTrackFrom(positionLinkedList.size());

			// Getting a list of all the neighbors, @see getAvailableDirections.
			List<Position> notVisitedNeighbors = maze3d.getLegalMoves(positionLinkedList.get(positionsIndexToContinueTrackFrom));

			// Removing all the directions that we already visited at.
			visitedPositions.forEach(notVisitedNeighbors::remove);

			// If positionLinkedList.get(positionsIndexToContinueTrackFrom) has no available neighbors...
			if (notVisitedNeighbors.isEmpty()) {

				// Going back in the positions list, looking for position with unvisited neighbors
				positionLinkedList.remove(positionsIndexToContinueTrackFrom);
			}
			// Adding to the track random neighbor of $positionLinkedList.get(positionsIndexToContinueTrackFrom)
			else {
				// Paving the track towards towards
				// random neighbor of $positionLinkedList.get(positionsIndexToContinueTrackFrom)
				paveTrack(
						positionLinkedList.get(positionsIndexToContinueTrackFrom),
						// Randomly choosing a neighbor of of $positionLinkedList.get(positionsIndexToContinueTrackFrom)
						notVisitedNeighbors.get(randomNumbersGenerator.nextInt(notVisitedNeighbors.size())),
						positionLinkedList, visitedPositions, maze3d);
			}
		}
		return maze3d;
	}

	abstract int getPositionsIndexToContinueTrackFrom(int positionListLength);

	/**
	 * Adding a new position to the track.
	 *
	 * @pre $oldPosition & $newPosition are neighbors!
	 *
	 * @param oldPosition        position.Position that has already paved (we came from there).
	 * @param newPosition        The new position.Position that we want to add to the track
	 * @param positionLinkedList The positions Stack from the Growing-Tree algorithm
	 * @param maze3d             The played maze.
	 */
	private void paveTrack(Position oldPosition, Position newPosition,
						   LinkedList<Position> positionLinkedList, Set<Position> visitedPositions,
						   Maze3d maze3d) {

		positionLinkedList.push(newPosition);
		visitedPositions.add(newPosition);

		// Deleting the barrier between the two neighbors, $oldPosition & $newPosition
        maze3d.setBlockStatus(Maze3d.findWallBetweenPositions(oldPosition, newPosition), Maze3d.PASS);
    }
}
