package maze_generators;


import position.Position;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class RandomizedKruskalsGenerator extends AbstractMaze3dGenerator {

    private Maze3d maze3d;
    private List<Position> walls;
    private Set<Set<Position>> positionsGroups;

    private Set<Position> visitedPosition;


    @Override
    public Maze3d generate(int mazeHeight, int mazeWidth, int mazeDepth) {
        maze3d = new Maze3d(mazeHeight, mazeWidth, mazeDepth);
        walls = new ArrayList<>();
        positionsGroups = new HashSet<>();
        visitedPosition = new HashSet<>();

        addSurroundWalls();
        initializePositionsGroups();

        for (int wallCount = 0; wallCount < walls.size(); wallCount++) {
            final Position randomWall = walls.get(randomNumbersGenerator.nextInt(walls.size()));
            Position[] positions = Maze3d.findPositionDevidedByWall(randomWall);
            Set<Position> setContainPositionInFirstSide = findSetContainPosition(positions[0]);
            Set<Position> setContainPositionInSecondSide = findSetContainPosition(positions[1]);

            if (setContainPositionInFirstSide != setContainPositionInSecondSide) {
                maze3d.setBlockStatus(randomWall, Maze3d.PASS);
                //noinspection ConstantConditions
                if (setContainPositionInFirstSide.size() > setContainPositionInSecondSide.size()) {
                    setContainPositionInFirstSide.addAll(setContainPositionInSecondSide);
                    positionsGroups.remove(setContainPositionInSecondSide);
                } else {
                    setContainPositionInSecondSide.addAll(setContainPositionInFirstSide);
                    positionsGroups.remove(setContainPositionInFirstSide);
                }

            }
        }
        return maze3d;
    }

    private void addSurroundWalls() {
        goThroughAllPositions(currentPosition ->
                walls.addAll(maze3d.getLegalMoves(currentPosition).stream()
                        .map(neighbor -> Maze3d.findWallBetweenPositions(currentPosition, neighbor))
                        .collect(Collectors.toList())));
    }

    private void initializePositionsGroups() {

        goThroughAllPositions(position -> {

            Set<Position> singleton = new HashSet<>();
            singleton.add(position);
            positionsGroups.add(singleton);
        });
    }

    private void goThroughAllPositions(Consumer<Position> positionConsumer) {
        for (int heightIndex = 0; heightIndex < maze3d.getMazeHeight(); heightIndex++) {
            for (int widthIndex = 0; widthIndex < maze3d.getMazeWidth(); widthIndex++) {
                for (int depthIndex = 0; depthIndex < maze3d.getMazeDepth(); depthIndex++) {

                    positionConsumer.accept(new Position(heightIndex, widthIndex, depthIndex));
                }
            }
        }
    }

    private Set<Position> findSetContainPosition(Position position) {
        for (Set<Position> positionSet : positionsGroups) {
            if (positionSet.contains(position)) return positionSet;
        }
        return null;
    }
}
