package maze_generators;


public interface Maze3dGenerator {


	/**
	 * @pre 0 < $height, $width, $depth < Integer.MAX_VALUE
	 *
	 * @param mazeHeight    The height of the 3D-Maze
	 * @param mazeWidth     The width of the 3D-Maze
	 * @param mazeDepth     The depth of the 3D-Maze
	 *
	 *
	 * @return  Functional 3D-Maze with dimensions $mazeHeight X $mazeWidth X $mazeDepth
	 *
	 * @throws java.lang.OutOfMemoryError if the maze is too big for the computer to handle.
	 *
	 */
	Maze3d generate(int mazeHeight, int mazeWidth, int mazeDepth);


	/**
	 * @pre 0 < $height, $width, $depth < Integer.MAX_VALUE
	 *
	 * @param mazeHeight    The height of the 3D-Maze
	 * @param mazeWidth     The width of the 3D-Maze
	 * @param mazeDepth     The depth of the 3D-Maze
	 *
	 * @return  User-friendly prompt on how match time it took the algorithm to
	 *          generate $mazeHeight X $mazeWidth X $mazeDepth maze
	 *
	 * @throws java.lang.OutOfMemoryError if the maze is too big for the computer to handle.
	 */
	String measureAlgorithmTime(int mazeHeight, int mazeWidth, int mazeDepth);
}
