package maze_generators;

import position.Position;
import search.Searchable;
import search.State;

import java.util.ArrayList;
import java.util.List;


public class SearchableMaze3d implements Searchable<Position> {

	private final Maze3d maze3d;

	public SearchableMaze3d(Maze3d build3dMaze) {
		this.maze3d = build3dMaze;
	}

	@Override
	public State<Position> getStartState() {
		return new State<>(this.maze3d.getStartPosition());
	}

	@Override
	public State<Position> getGoalState() {
		return new State<>(this.maze3d.getGoalPosition());
	}

	@Override
	public List<State<Position>> getAllPossibleStates(State<Position> currentPosition) {
		List<State<Position>> possibleMovePositions = new ArrayList<>();
		for (Position move : this.maze3d.getPossibleMovesByPositions(currentPosition.getState())) {
			possibleMovePositions.add(new State<>(move, 1));
		}
		return possibleMovePositions;
	}

	@Override
	public String toString() {
		return this.maze3d.toString();
	}
}
