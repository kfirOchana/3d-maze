package maze_generators;


public class GrowingTreeGenerator_RandomSection extends AbstractGrowingTreeGenerator {

	@Override
	int getPositionsIndexToContinueTrackFrom(int positionListLength) {
		return randomNumbersGenerator.nextInt(positionListLength);
	}
}
