package position;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

/**
 * This class represent coordinates set in the n-Dimensional space.
 */
public class Position implements Serializable {

	private final int[] dimensionsPosition;

	/**
	 * Constructor that takes several positions, one for each dimension.
	 * USE EXAMPLE: new position.Position(5, 2, 6, 2, 4)
	 * @param dimensionsPosition    list of the position in each dimension,
	 *                              i.e dimensionsPosition[x] is our position in the x-Axis
	 */
	public Position(int... dimensionsPosition) {
		this.dimensionsPosition = dimensionsPosition;
	}

	public static Position add(Position p1, Position p2) {
        return binaryOperator(p1, p2, (arg1, arg2) -> arg1 + arg2);
    }

	/**
	 * @pre p1.getNumberOfDimensions() == p2.getNumberOfDimensions()
	 *
	 * @param p1    first position.Position argument
	 * @param p2    second position.Position argument
	 * @param binaryOperator    (int, int) -> int function
	 * @return new position.Position with coordinates generated from p1 & p2 using binaryOperator.
	 */
    public static Position binaryOperator(Position p1, Position p2, BinaryOperator<Integer> binaryOperator) {
        return new Position(mergeDimensionsWithOperator(p1, p2, binaryOperator));
    }

    public static Position unaryOperator(Position p, UnaryOperator<Integer> unaryOperator) {
        int[] dimensionsAfterCalc = new int[p.dimensionsPosition.length];
        for (int dimensionIndex = 0; dimensionIndex < p.dimensionsPosition.length; dimensionIndex++) {
            dimensionsAfterCalc[dimensionIndex] = unaryOperator.apply(p.dimensionsPosition[dimensionIndex]);
        }
        return new Position(dimensionsAfterCalc);
    }

	/**
	 *
	 * @param p1    first position.Position argument
	 * @param p2    second position.Position argument
	 * @param binaryOperator    (int, int) -> int function
	 * @return int array s.t $ret[i] = binaryOperator(p1.getLocationAtDimension(i), p2.getLocationAtDimension(i)
	 *          for all 0 <= i < p1.getNumberOfDimensions()
	 */
	private static int[] mergeDimensionsWithOperator(Position p1, Position p2, BinaryOperator<Integer> binaryOperator) {
		if (p1.dimensionsPosition.length != p2.dimensionsPosition.length) throw new IllegalArgumentException();
		int[] dimensionsAfterCalc = new int[p1.dimensionsPosition.length];
		for (int dimensionIndex = 0; dimensionIndex < p1.dimensionsPosition.length; dimensionIndex++) {
			dimensionsAfterCalc[dimensionIndex] =
					binaryOperator.apply(p1.dimensionsPosition[dimensionIndex], p2.dimensionsPosition[dimensionIndex]);
		}
		return dimensionsAfterCalc;
	}

	/**
	 * Getter for the dimensions position.
	 * @param dimensionIndex    The index for the dimension that we require.
	 * @return The position in the $dimensionIndex axis
	 *
	 * @throws IndexOutOfBoundsException if not 0 <= dimensionIndex < n (i.e the number of dimensions)
	 */
	public int getLocationAtDimension(int dimensionIndex) {
		return this.dimensionsPosition[dimensionIndex];
	}

	/**
	 * @return The number of dimensions that we are located at.
	 */
	public int getNumberOfDimensions() {
		return this.dimensionsPosition.length;
	}

	public int[] toArray() {
		return this.dimensionsPosition.clone();
	}

	/**
	 * This code is taken from Arrays.toString(int[]) and modified to our format.
	 */
	@Override
	public String toString() {

		if (this.dimensionsPosition == null)
			return "null";
		int iMax = getNumberOfDimensions() - 1;
		if (iMax == -1)
			return "{}";

		StringBuilder toStringBuilder = new StringBuilder();
		toStringBuilder.append('{');
		for (int i = 0; ; i++) {
			toStringBuilder.append(getLocationAtDimension(i));
			if (i == iMax)
				return toStringBuilder.append('}').toString();
			toStringBuilder.append(", ");
		}
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Position && Arrays.equals(this.dimensionsPosition, ((Position) obj).dimensionsPosition);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.dimensionsPosition);
	}
}
